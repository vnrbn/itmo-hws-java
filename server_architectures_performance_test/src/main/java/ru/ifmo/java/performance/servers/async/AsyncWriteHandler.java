package ru.ifmo.java.performance.servers.async;

import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public class AsyncWriteHandler implements CompletionHandler<Integer, AsyncClientInfo> {
    private final ByteBuffer buffer;

    public AsyncWriteHandler(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void completed(Integer result, AsyncClientInfo clientInfo) {
        if (!clientInfo.getChannel().isOpen()) {
            return;
        }
        // System.out.println("from write handler: " + result + " -- " + Thread.currentThread().toString());
        if (buffer.hasRemaining()) {
            clientInfo.getChannel().write(buffer, clientInfo, this);
        } else {
            // client request processed and send - timer stop
            PerformanceTimer receivedSendTimer = clientInfo.getMyReceiveSendTimers().poll();
            if (receivedSendTimer != null) {
                receivedSendTimer.stop();
                clientInfo.getGlobalReceiveSendTimers().add(receivedSendTimer);
            }
        }
    }

    @Override
    public void failed(Throwable exc, AsyncClientInfo attachment) {

    }
}
