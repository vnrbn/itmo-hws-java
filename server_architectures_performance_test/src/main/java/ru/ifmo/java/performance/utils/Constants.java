package ru.ifmo.java.performance.utils;

public class Constants {
    public static final int SERVER_PORT = 8765;

    public static final int SERVER_TASKS_POOL_SIZE = 10;
}
