package ru.ifmo.java.performance.gui;

import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.config.ConfigParameter;
import ru.ifmo.java.performance.config.ParameterType;
import ru.ifmo.java.performance.config.ServerType;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class PerformanceGUI implements Runnable {
    private final String GUI_NAME = "Server Architectures Performance Tester - Ivan Rybin MA SE ITMO & JB";

    private final int DIR_SELECT_FRAME_WIDTH = 650;
    private final int DIR_SELECT_FRAME_HEIGHT = 200;

    private final int SERVER_SELECT_FRAME_WIDTH = 650;
    private final int SERVER_SELECT_FRAME_HEIGHT = 300;

    private final int DYNAMIC_PARAM_SELECT_FRAME_WIDTH = 650;
    private final int DYNAMIC_PARAM_SELECT_FRAME_HEIGHT = 400;

    private final int PROGRESS_FRAME_WIDTH = 650;
    private final int PROGRESS_FRAME_HEIGHT = 200;

    private String loadFolderPath = null;
    private ServerType serverType = null;
    private ParameterType dynamicParameterType = ParameterType.CLIENTS_NUMBER;
    private ArrayList<ParameterType> staticParametersTypes = new ArrayList<>(2);
    private Long dynamicBeg = null;
    private Long dynamicEnd = null;
    private Long dynamicStep = null;
    private Config config = null;

    private final JFrame dirSelectFrame = new JFrame();
    private final JFrame serverSelectFrame = new JFrame();
    private final JLabel serverSelectLabel = new JLabel();
    private final JPanel serverSelectPanel = new JPanel();
    private final JFrame dynamicParamSelectFrame = new JFrame();
    private final JLabel dynamicParamSelectLabel = new JLabel();
    private final JLabel dynamicParamSelectErrorLabel = new JLabel();
    private final JFrame staticParamFrame = new JFrame();
    private final JLabel staticParamErrorLabel = new JLabel();
    private final JFrame progressFrame = new JFrame();
    private final JLabel progressLabel = new JLabel();

    private final JTextField begTf = new JTextField(15);
    private final JTextField endTf = new JTextField(15);
    private final JTextField stepTf = new JTextField(15);

    private final JTextField requestsNumberTf = new JTextField(15);
    private final JTextField tasksPoolSizeTf = new JTextField(15);
    private final JTextField staticParameter1Tf = new JTextField(15);
    private final JTextField staticParameter2Tf = new JTextField(15);

    private final ExecutorService mainThread = Executors.newSingleThreadExecutor();
    private final ExecutorService progressThread = Executors.newSingleThreadExecutor();

    private final GuiTesterProvider guiTesterProvider;

    private enum TextErrors {
        GOOD,
        EMPTY,
        ZERO,
        NEGATIVE,
        NOT_NUMBER,
        END_LOWER_BEG
    }

    public PerformanceGUI(GuiTesterProvider guiTesterProvider) {
        this.guiTesterProvider = guiTesterProvider;
    }

    @Override
    public void run() {
        mainThread.submit(this::loadDirSelectPage);
    }
    
    private void awaitTesterReady() {
        Lock testerLock = guiTesterProvider.getTesterLock();
        Condition testerEvent = guiTesterProvider.getTesterEvent();
        try {
            testerLock.lockInterruptibly();
            while (!guiTesterProvider.isTestReady()) {
                testerEvent.await(60, TimeUnit.SECONDS);
                ConfigParameter dynamicParam = config.getParam(config.dynamicParameterType);
                if (dynamicEnd - dynamicBeg != 0) {
                    double progress = (double) (dynamicParam.currValue.get() - dynamicBeg) / (dynamicEnd - dynamicBeg);
                    String percents = String.format("%.2f", progress * 100);
                    if (percents.equals("100,00")) {
                        progressLabel.setText("progress: " + percents + "% - wait dump");
                    } else {
                        progressLabel.setText("progress: " + percents + "%");
                    }
                }
            }
            progressLabel.setText("performance tests done - close window");
        } catch (InterruptedException e) {
            progressFrame.dispose();
        } finally {
            testerLock.unlock();
        }
    }

    private void notifyGUIReady() {
        guiTesterProvider.setConfig(config);
        guiTesterProvider.setGuiReady(true);
        guiTesterProvider.notifyGuiEvent();
    }

    private void loadProgressBar() {
        progressFrame.setTitle(GUI_NAME);
        progressFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        progressFrame.setSize(PROGRESS_FRAME_WIDTH, PROGRESS_FRAME_HEIGHT);
        progressFrame.setLayout(new GridLayout(1, 1));
        initProgressLabel();
        progressThread.submit(this::awaitTesterReady);
        progressFrame.setLocation(getFrameCenterPoint(progressFrame));
        progressFrame.setVisible(true);
    }

    private void loadStaticParametersPage() {
        staticParamFrame.setTitle(GUI_NAME);
        staticParamFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        staticParamFrame.setSize(DYNAMIC_PARAM_SELECT_FRAME_WIDTH, DYNAMIC_PARAM_SELECT_FRAME_HEIGHT);
        staticParamFrame.setLayout(new GridLayout(6, 1));
        initStaticErrorLabel();
        initStaticParamValues();
        initStaticParamSubmitButton();
        staticParamFrame.setLocation(getFrameCenterPoint(staticParamFrame));
        staticParamFrame.setVisible(true);
    }

    private void loadDynamicParameterSelectPage() {
        dynamicParamSelectFrame.setTitle(GUI_NAME);
        dynamicParamSelectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dynamicParamSelectFrame.setSize(DYNAMIC_PARAM_SELECT_FRAME_WIDTH, DYNAMIC_PARAM_SELECT_FRAME_HEIGHT);
        dynamicParamSelectFrame.setLayout(new GridLayout(5, 1));
        initDynamicParamSelectLabels();
        initComboBox();
        initDynamicParamValues();
        initDynamicParamSubmitButton();
        dynamicParamSelectFrame.setLocation(getFrameCenterPoint(dynamicParamSelectFrame));
        dynamicParamSelectFrame.setVisible(true);
    }

    private void loadServerSelectPage() {
        serverSelectFrame.setTitle(GUI_NAME);
        serverSelectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        serverSelectFrame.setSize(SERVER_SELECT_FRAME_WIDTH, SERVER_SELECT_FRAME_HEIGHT);
        serverSelectFrame.setLayout(new GridLayout(2, 1));
        serverSelectPanel.setLayout(new GridLayout(3, 1));
        initServerSelectLabel();
        for (ServerType type : ServerType.values()) {
            initServerSelectButton(type);
        }
        serverSelectFrame.getContentPane().add(serverSelectPanel);
        serverSelectFrame.setLocation(getFrameCenterPoint(serverSelectFrame));
        serverSelectFrame.setVisible(true);
    }

    private void initProgressLabel() {
        Font font = new Font(null, Font.PLAIN, 30);
        initLabel(font, "progress: 0.00%", Color.GREEN, progressLabel, progressFrame);
    }

    private void initComboBox() {
        JPanel comboBoxPanel = new JPanel();
        comboBoxPanel.setSize(DYNAMIC_PARAM_SELECT_FRAME_WIDTH, 50);
        String[] params = new String[] {
            ParameterType.CLIENTS_NUMBER.toString(),
            ParameterType.ARRAY_SIZE.toString(),
            ParameterType.REQUEST_DELTA_TIME.toString()
        };
        JComboBox<String> paramsList = new JComboBox<>(params);
        paramsList.addActionListener(actionEvent -> {
            JComboBox<String> cb = (JComboBox<String>) actionEvent.getSource();
            dynamicParameterType = ParameterType.getByToString((String) Objects.requireNonNull(cb.getSelectedItem()));
        });
        paramsList.setSize(200, 50);
        comboBoxPanel.add(paramsList);
        dynamicParamSelectFrame.getContentPane().add(comboBoxPanel);
    }

    private void initStaticErrorLabel() {
        Font font = new Font(null, Font.PLAIN, 15);
        initLabel(font, null, Color.RED, staticParamErrorLabel, staticParamFrame);
    }

    private void initStaticParamSubmitButton() {
        JPanel submitPanel = new JPanel();
        JButton button = new JButton("submit");
        button.addActionListener(actionEvent -> {
            TextErrors error = checkLongField(requestsNumberTf);
            if (error == TextErrors.GOOD) {
                Long value = parseLong(requestsNumberTf);
                // new static parameter
                config.setRequestsNumber(value.intValue());
            } else {
                updateErrorLabel("requests number", error, staticParamErrorLabel);
                return;
            }
            error = checkLongField(tasksPoolSizeTf);
            if (error == TextErrors.GOOD) {
                Long value = parseLong(tasksPoolSizeTf);
                // new static parameter
                config.setTasksPoolSize(value.intValue());
            } else {
                updateErrorLabel("tasks pool size", error, staticParamErrorLabel);
                return;
            }
            if (!setStaticParameter(0) || !setStaticParameter(1)) {
                return;
            }
            staticParamFrame.dispose();
            // GO TESTER
            notifyGUIReady();
            // next frame
            mainThread.submit(this::loadProgressBar);
        });
        submitPanel.add(button);
        staticParamFrame.getContentPane().add(submitPanel);
    }

    private boolean setStaticParameter(int number) {
        JTextField sParam = null;
        if (number == 0) {
            sParam = staticParameter1Tf;
        } else if (number == 1) {
            sParam = staticParameter2Tf;
        }
        TextErrors error = checkLongField(sParam);
        ParameterType pType = staticParametersTypes.get(number);
        if (maybeGoodOrGoodZero(error, pType)) {
            Long value = parseLong(sParam);
            // new static parameter
            config.setStaticParameter(pType, value);
        } else {
            updateErrorLabel(pType.toString(), error, staticParamErrorLabel);
            return false;
        }
        return true;
    }

    private boolean maybeGoodOrGoodZero(TextErrors error, ParameterType pType) {
        if (error == TextErrors.GOOD) {
            return true;
        }
        if (error == TextErrors.ZERO) {
            return pType.equals(ParameterType.REQUEST_DELTA_TIME) || pType.equals(ParameterType.ARRAY_SIZE);
        }
        return false;
    }

    private void initDynamicParamSubmitButton() {
        JPanel submitPanel = new JPanel();
        JButton button = new JButton("submit");
        button.addActionListener(actionEvent -> {
            TextErrors error = checkLongField(begTf);
            if (error == TextErrors.GOOD) {
                dynamicBeg = parseLong(begTf);
            } else {
                updateErrorLabel("beg", error, dynamicParamSelectErrorLabel);
                return;
            }
            error = checkLongField(endTf);
            if (error == TextErrors.GOOD) {
                dynamicEnd = parseLong(endTf);
            } else {
                updateErrorLabel("end", error, dynamicParamSelectErrorLabel);
                return;
            }
            if (dynamicBeg > dynamicEnd) {
                updateErrorLabel("end", TextErrors.END_LOWER_BEG, dynamicParamSelectErrorLabel);
                return;
            }
            error = checkLongField(stepTf);
            if (error == TextErrors.GOOD) {
                dynamicStep = parseLong(stepTf);
            } else {
                updateErrorLabel("step", error, dynamicParamSelectErrorLabel);
                return;
            }
            // new config
            config = new Config(serverType, dynamicParameterType);
            config.setLoadFolderPath(loadFolderPath);
            config.setDynamicParameter(config.dynamicParameterType, dynamicBeg, dynamicEnd, dynamicStep);
            Arrays.stream(ParameterType.values())
                    .filter(type -> !type.equals(dynamicParameterType))
                    .forEach(type -> staticParametersTypes.add(type));
            dynamicParamSelectFrame.dispose();
            // next frame
            mainThread.submit(this::loadStaticParametersPage);
        });
        submitPanel.add(button);
        dynamicParamSelectFrame.getContentPane().add(submitPanel);
    }

    private void updateErrorLabel(String fieldName, TextErrors error, JLabel label) {
        String message = fieldName;
        switch (error) {
            case ZERO: {
                message += " must be non zero";
                break;
            }
            case NEGATIVE: {
                message += " must be positive";
                break;
            }
            case NOT_NUMBER: {
                message = " not a number";
                break;
            }
            case EMPTY: {
                message = " is empty";
                break;
            }
            case END_LOWER_BEG: {
                message += " must be greater than beg";
            }
            default: {
                break;
            }
        }
        label.setText(message);
    }

    private Long parseLong(JTextField textField) {
        try {
            return Long.parseLong(textField.getText());
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private TextErrors checkLongField(JTextField textField) {
        String text = textField.getText();
        if (text.isEmpty()) {
            return TextErrors.EMPTY;
        }
        Long value = parseLong(textField);
        if (value == null) {
            return TextErrors.NOT_NUMBER;
        }
        if (value < 0) {
            return TextErrors.NEGATIVE;
        }
        if (value == 0) {
            return TextErrors.ZERO;
        }
        return TextErrors.GOOD;
    }

    private void initDynamicParamValues() {
        JPanel valuesPanel = new JPanel();

        JLabel beg = new JLabel();
        JLabel end = new JLabel();
        JLabel step = new JLabel();

        beg.setVerticalAlignment(JLabel.CENTER);
        end.setHorizontalAlignment(JLabel.CENTER);
        step.setHorizontalAlignment(JLabel.CENTER);

        begTf.setHorizontalAlignment(JTextField.RIGHT);
        endTf.setHorizontalAlignment(JTextField.RIGHT);
        stepTf.setHorizontalAlignment(JTextField.RIGHT);

        beg.setText("beg");
        end.setText("end");
        step.setText("step");

        valuesPanel.add(beg);
        valuesPanel.add(begTf);
        valuesPanel.add(end);
        valuesPanel.add(endTf);
        valuesPanel.add(step);
        valuesPanel.add(stepTf);

        dynamicParamSelectFrame.getContentPane().add(valuesPanel);
    }

    private void initStaticParamValues() {
        JPanel requestPanel1 = createLabelPanel("requests number", null);
        JPanel tasksPanel1 = createLabelPanel("tasks pool size", null);
        JPanel sParam1Panel1 = createLabelPanel(staticParametersTypes.get(0).toString(), null);
        JPanel sParam2Panel1 = createLabelPanel(staticParametersTypes.get(1).toString(), null);

        JPanel requestPanel2 = createInputValuePanel(requestsNumberTf, null);
        JPanel tasksPanel2 = createInputValuePanel(tasksPoolSizeTf, null);
        JPanel sParam1Panel2 = createInputValuePanel(staticParameter1Tf, null);
        JPanel sParam2Panel2 = createInputValuePanel(staticParameter2Tf, null);

        createDoublePanel(requestPanel1, requestPanel2, staticParamFrame);
        createDoublePanel(tasksPanel1, tasksPanel2, staticParamFrame);
        createDoublePanel(sParam1Panel1, sParam1Panel2, staticParamFrame);
        createDoublePanel(sParam2Panel1, sParam2Panel2, staticParamFrame);
    }

    private JPanel createDoublePanel(JPanel panel1, JPanel panel2, JFrame frame) {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(1, 2));
        panel.add(panel1);
        panel.add(panel2);
        if (frame != null) {
            frame.getContentPane().add(panel);
        }
        return panel;
    }

    private JPanel createInputValuePanel(JTextField textField, JPanel jPanel) {
        if (jPanel == null) {
            jPanel = new JPanel();
        }
        textField.setHorizontalAlignment(JTextField.RIGHT);
        jPanel.add(textField);
        return jPanel;
    }

    private JPanel createLabelPanel(String label, JPanel jPanel) {
        JLabel jLabel = new JLabel(label);
        jLabel.setVerticalAlignment(JLabel.CENTER);
        if (jPanel == null) {
            jPanel = new JPanel();
        }
        jPanel.add(jLabel);
        return jPanel;
    }

    private void initLabel(Font font, String text, Color color, JLabel label, JFrame frame) {
        label.setVerticalAlignment(JLabel.CENTER);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setFont(font);
        frame.getContentPane().add(label);
        if (color != null) {
            label.setForeground(color);
        }
        if (text != null) {
            label.setText(text);
        }
        if (font != null) {
            label.setFont(font);
        }
    }

    private void initDynamicParamSelectLabels() {
        Font font = new Font(null, Font.PLAIN, 15);
        initLabel(font, null, Color.RED, dynamicParamSelectErrorLabel, dynamicParamSelectFrame);
        initLabel(font, "choose dynamic parameter and set [beg, end, step] values",
                null, dynamicParamSelectLabel, dynamicParamSelectFrame
        );
    }

    private void initServerSelectLabel() {
        Font font = new Font(null, Font.PLAIN, 20);
        serverSelectLabel.setText("choose server architecture");
        serverSelectLabel.setVerticalAlignment(JLabel.CENTER);
        serverSelectLabel.setHorizontalAlignment(JLabel.CENTER);
        serverSelectLabel.setFont(font);
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(1, 1));
        labelPanel.add(serverSelectLabel);
        serverSelectFrame.getContentPane().add(labelPanel);
    }

    private void loadDirSelectPage() {
        dirSelectFrame.setTitle(GUI_NAME);
        dirSelectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dirSelectFrame.setSize(DIR_SELECT_FRAME_WIDTH, DIR_SELECT_FRAME_HEIGHT);
        initDirSelectButton();
        dirSelectFrame.setLocation(getFrameCenterPoint(dirSelectFrame));
        dirSelectFrame.setVisible(true);
    }

    private void initServerSelectButton(ServerType serverType) {
        JButton button = new JButton(serverType.toString());
        button.addActionListener(actionEvent -> {
            this.serverType = serverType;
            serverSelectFrame.dispose();
            // next frame
            mainThread.submit(this::loadDynamicParameterSelectPage);
        });
        serverSelectPanel.add(button);
    }

    private void initDirSelectButton() {
        JButton button = new JButton("select dump folder");
        button.addActionListener(actionEvent -> {
            File folder = loadWithChooser(JFileChooser.DIRECTORIES_ONLY);
            if (folder != null) {
                loadFolderPath = folder.getAbsolutePath();
                dirSelectFrame.dispose();
                // next frame
                mainThread.submit(this::loadServerSelectPage);
            }
        });
        dirSelectFrame.add(button);
    }

    private File loadWithChooser(int FILES_TYPE) {
        JFileChooser f = new JFileChooser();
        f.setCurrentDirectory(new File(System.getProperty("user.dir")));
        f.setFileSelectionMode(FILES_TYPE);
        f.showSaveDialog(null);
        return f.getSelectedFile();
    }

    private Point getFrameCenterPoint(JFrame frame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point mid = new Point(screenSize.width / 2, screenSize.height / 2);
        return new Point(mid.x - (frame.getWidth() / 2),mid.y - (frame.getHeight() / 2));
    }
}
