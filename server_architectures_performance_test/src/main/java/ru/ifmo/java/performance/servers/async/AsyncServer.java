package ru.ifmo.java.performance.servers.async;

import ru.ifmo.java.performance.servers.Server;
import ru.ifmo.java.performance.utils.Constants;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncServer implements Server {
    private final CountDownLatch latch;

    private final ConcurrentLinkedQueue<PerformanceTimer> sortTimers = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<PerformanceTimer> receiveSendTimers = new ConcurrentLinkedQueue<>();

    private final ExecutorService tasksPool;

    private AsynchronousServerSocketChannel asyncServerSocketChannel;

    private volatile boolean isInterrupted = false;

    public AsyncServer(CountDownLatch latch, int tasksPoolSize) {
        this.tasksPool = Executors.newFixedThreadPool(tasksPoolSize);
        this.latch = latch;
        try {
            this.asyncServerSocketChannel = AsynchronousServerSocketChannel.open()
                    .bind(new InetSocketAddress("localhost", Constants.SERVER_PORT));
        } catch (IOException e) {
            System.out.println("SERVER: INIT ERROR");
        }
    }

    @Override
    public void run() {
        latch.countDown();
        while (!isInterrupted) {
            asyncServerSocketChannel.accept(null , new CompletionHandler<AsynchronousSocketChannel, Object>() {
                @Override
                public void completed(AsynchronousSocketChannel clientChannel, Object attachment) {
                    if (asyncServerSocketChannel.isOpen()) {
                        asyncServerSocketChannel.accept(null, this);
                    }
                    if (clientChannel != null && clientChannel.isOpen()) {
                        try {
                            System.out.println("new client connected " + clientChannel.getRemoteAddress().toString() + " " + Thread.currentThread().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        AsyncReadHandler readHandler = new AsyncReadHandler();
                        AsyncClientInfo clientInfo = new AsyncClientInfo(clientChannel, sortTimers, receiveSendTimers, tasksPool);
                        clientChannel.read(clientInfo.getSizeBuffer(), clientInfo, readHandler);
                    }
                }

                @Override
                public void failed(Throwable exc, Object attachment) {
                    if (!isInterrupted) {
                        System.err.println("SERVER: ACCEPT HANDLER - CONNECTION ERROR");
                        exc.printStackTrace();
                    }
                }
            });
        }
        System.out.println("[X] - SERVER DONE");
    }

    @Override
    public void shutDown() {
        isInterrupted = true;
        if (asyncServerSocketChannel != null && asyncServerSocketChannel.isOpen()) {
            try {
                asyncServerSocketChannel.close();
            } catch (IOException e) {
                // ignore
            }
        }
        tasksPool.shutdownNow();
    }

    @Override
    public Queue<PerformanceTimer> getSortTimers() {
        return sortTimers;
    }

    @Override
    public Queue<PerformanceTimer> getReceiveSendTimers() {
        return receiveSendTimers;
    }
}
