package ru.ifmo.java.performance.graphics;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.config.ParameterType;
import ru.ifmo.java.performance.tester.PerformanceResults;

import java.awt.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.List;

public class PlotBuilder {
    private static final String HEADER_CLIENT = "mean request process time on client";
    private static final String HEADER_SORT = "mean bubble sort process time on server";
    private static final String HEADER_REQUEST = "mean client request process time on server";

    private static final String SHORT_CLIENT = "request on client";
    private static final String SHORT_SORT = "bubble sort on server";
    private static final String SHORT_REQUEST = "request on server";

    private static final String Y_AXIS_LABEL = "time (seconds)";
    private static final String X_AXIS_ARRAY_SIZE = "array size (# elements of int type)";
    private static final String X_AXIS_CLIENTS_NUMBER = "clients number";
    private static final String X_AXIS_REQUEST_DELTA_TIME = "delta time between client requests (milliseconds)";

    public static List<JFreeChart> buildPlotForEveryMetric(Config config, PerformanceResults results) {
        List<JFreeChart> plots = new LinkedList<>();
        List<Map.Entry<Long, Double>> sortStats = results.getSortStats();
        List<Map.Entry<Long, Double>> receivedSendStats = results.getReceivedSendStats();
        List<Map.Entry<Long, Double>> clientsStats = results.getClientsStats(config.requestsNumber);

        XYDataset clientsDataset = createDataset(createSeries(clientsStats, SHORT_CLIENT));
        XYDataset sortDataset = createDataset(createSeries(sortStats, SHORT_SORT));
        XYDataset receivedSendDataset = createDataset(createSeries(receivedSendStats, SHORT_REQUEST));
        XYDataset summaryPlot = createDataset(Arrays.asList(
                createSeries(sortStats, SHORT_SORT),
                createSeries(receivedSendStats, SHORT_REQUEST),
                createSeries(clientsStats, SHORT_CLIENT)
        ));

        String xLabel = getXAxisLabel(config.dynamicParameterType);
        String serverLabel = config.serverType.toString();

        plots.add(createChart(sortDataset, serverLabel + " server - " + HEADER_SORT, xLabel, false));
        plots.add(createChart(receivedSendDataset, serverLabel + " server - " + HEADER_REQUEST, xLabel, false));
        plots.add(createChart(clientsDataset, serverLabel + " server - " + HEADER_CLIENT, xLabel, false));
        plots.add(createChart(summaryPlot, serverLabel + " server", xLabel, true));

        return plots;
    }

    public static String getXAxisLabel(ParameterType dynamicParamType) {
        switch (dynamicParamType) {
            case CLIENTS_NUMBER: return X_AXIS_CLIENTS_NUMBER;
            case ARRAY_SIZE: return X_AXIS_ARRAY_SIZE;
            case REQUEST_DELTA_TIME: return X_AXIS_REQUEST_DELTA_TIME;
            default: return null;
        }
    }

    public static XYDataset createDataset(XYSeries series) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series);
        return dataset;
    }

    public static XYDataset createDataset(List<XYSeries> seriesList) {
        XYSeriesCollection dataset = new XYSeriesCollection();
        seriesList.forEach(dataset::addSeries);
        return dataset;
    }

    public static XYSeries createSeries(List<Map.Entry<Long, Double>> data, String name) {
        XYSeries series = new XYSeries(name);
        data.forEach(entry -> {
            long value = entry.getKey();
            double time = entry.getValue();
            series.add(value, time);
        });
        return series;
    }

    public static JFreeChart createChart(XYDataset dataset, String header, String xLabel, boolean isLegend) {
        JFreeChart chart = ChartFactory.createXYLineChart(
                header,
                xLabel,
                Y_AXIS_LABEL,
                dataset,
                PlotOrientation.VERTICAL,
                isLegend,
                true,
                false
        );

        chart.setBackgroundPaint(Color.white);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint    (Color.lightGray);
        plot.setDomainGridlinePaint(Color.white    );
        plot.setRangeGridlinePaint (Color.white    );

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
            renderer.setDefaultShapesFilled(true);
            renderer.setDefaultShapesVisible(true);
            renderer.setDrawSeriesLineAsPath(true);
        }
        return chart;
    }

    public static boolean dumpAsPNG(String path, JFreeChart chart, int width, int height) {
        try {
            OutputStream out = new FileOutputStream(path + ".png");
            ChartUtils.writeChartAsPNG(out, chart, width, height);
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
