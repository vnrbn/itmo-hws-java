package ru.ifmo.java.performance.servers.async;

import ru.ifmo.java.performance.utils.BubbleSort;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class AsyncSortTask implements Runnable {
    private static AtomicInteger taskId = new AtomicInteger(0);
    private final int id = taskId.incrementAndGet();

    private final AsyncClientInfo clientInfo;
    private final List<Integer> values;
    private final PerformanceTimer sortTimer = new PerformanceTimer();

    private volatile boolean isReady = false;

    public AsyncSortTask(
            AsyncClientInfo clientInfo,
            List<Integer> values
    ) {
        this.clientInfo = clientInfo;
        this.values = values;
    }

    @Override
    public void run() {
        // System.out.println("task " + id + ": running");

        // make job
        sortTimer.start();
        BubbleSort.sort(values);
        sortTimer.stop();

        // register sort time
        clientInfo.getSortTimers().add(sortTimer);

        // notify task ready
        isReady = true;
        clientInfo.writeMessage(values);
    }

    public List<Integer> getValues() {
        return values;
    }

    public boolean isReady() {
        return isReady;
    }

    public PerformanceTimer getSortTimer() {
        return sortTimer;
    }

    public static void resetCounter() {
        taskId.set(0);
    }
}
