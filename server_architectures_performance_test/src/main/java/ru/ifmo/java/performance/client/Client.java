package ru.ifmo.java.performance.client;

import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.Constants;
import ru.ifmo.java.performance.utils.ListFactory;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class Client implements Runnable, SortRequester {
    private static final AtomicInteger clientId = new AtomicInteger(0);
    private final int id = clientId.incrementAndGet();

    private final Config config;
    private final ArrayList<ArrayList<Integer>> data;
    private final CountDownLatch startLatch;
    private final CountDownLatch stopLatch;
    private final PerformanceTimer timer = new PerformanceTimer();

    private SocketChannel channel;

    public Client(Config config, CountDownLatch startLatch, CountDownLatch stopLatch) {
        this.config = config;
        this.data = new ArrayList<>(config.requestsNumber);
        for (int i = 0; i < config.requestsNumber; ++i) {
            this.data.add(ListFactory.genList((int) config.arraySize.getCurrentValue()));
        }
        this.startLatch = startLatch;
        this.stopLatch = stopLatch;
    }

    @Override
    public void run() {
        awaitOthers();
        timer.start();  // timer

        System.out.println("CLIENT " + id + " RUNNING");

        try {
            initSocket();
            for (int i = 0; i < config.requestsNumber; ++i) {
                sendRequest(getRequest(data.get(i)));
                Protocol.SortRequestResponse response = receiveResponse();

                if (response.getValuesList().size() >= 5) {
                    System.out.println("CLIENT " + id + " received: " +
                            response.getValuesList().subList(0, 5) + " + [...] of size " +
                            (config.arraySize.currValue.get() - 5L)
                    );
                } else {
                    System.out.println("CLIENT " + id + " received: " + response.getValuesList());
                }
                if (i + 1 != config.requestsNumber) {
                    Thread.sleep(config.requestDeltaTime.getCurrentValue());
                }
            }
            channel.close();
        } catch (InterruptedException e) {
            // ignore
        } catch (SocketException e) {
            System.out.println("CLIENT " + id + " SOCKET IS DEAD");
             e.printStackTrace();
        } catch (IOException e) {
            System.out.println("CLIENT " + id + " IO EXCEPTION");
            e.printStackTrace();
        }

        timer.stop();   // timer
        stopLatch.countDown();

        System.out.println("[X] - CLIENT " + id + " DONE ");
    }


    private void sendRequest(Protocol.SortRequestResponse request) throws IOException {
        ByteBuffer outSize = ByteBuffer.allocate(4);
        outSize.putInt(request.toByteArray().length);
        outSize.flip();

        ByteBuffer outMessage = ByteBuffer.wrap(request.toByteArray());
        channel.write(new ByteBuffer[]{outSize, outMessage});
    }

    private Protocol.SortRequestResponse receiveResponse() throws IOException {
        ByteBuffer inSize = ByteBuffer.allocate(4);
        channel.read(inSize);
        inSize.flip();

        ByteBuffer inMessage = ByteBuffer.allocate(inSize.getInt());
        while (inMessage.hasRemaining()) {
            channel.read(inMessage);
        }
        inMessage.flip();

        return Protocol.SortRequestResponse.parseFrom(inMessage);
    }

    private void initSocket() throws IOException {
        channel = SocketChannel.open(new InetSocketAddress("localhost", Constants.SERVER_PORT));
    }

    private void awaitOthers() {
        startLatch.countDown();
        try {
            startLatch.await(); // spurious wakeup's don't affect
        } catch (InterruptedException e) {
            System.err.println("CLIENT " + id + " interrupted, thread - " + Thread.currentThread().toString());
        }
    }

    public PerformanceTimer getTimer() {
        return timer;
    }

    public static void resetCounter() {
        clientId.set(0);
    }
}
