package ru.ifmo.java.performance.utils;

import java.util.concurrent.atomic.AtomicLong;

public class PerformanceTimer {
    private final AtomicLong start = new AtomicLong(0);
    private final AtomicLong stop = new AtomicLong(0);

    private volatile boolean isStopped = false;

    public PerformanceTimer() {}

    public PerformanceTimer(long start, long stop){
        this.start.set(start);
        this.stop.set(stop);
    }

    public void start() {
        start.set(System.nanoTime());
    }

    public void stop() {
        stop.set(System.nanoTime());
    }

    public long getDurationNanos() {
        return stop.get() - start.get();
    }

    public double getDurationSeconds() {
        return (double) (stop.get() - start.get()) / 1_000_000_000;
    }

    public PerformanceTimer getCopy() {
        return new PerformanceTimer(start.get(), stop.get());
    }

    public void reset() {
        start.set(0);
        stop.set(0);
    }

    public long getStart() {
        return start.get();
    }

    public long getStop() {
        return stop.get();
    }
}
