package ru.ifmo.java.performance.tester;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.config.MetricType;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class PerformanceResults {
    private final Map<Long, List<PerformanceTimer>> clientsResults = new HashMap<>();
    private final Map<Long, List<PerformanceTimer>> sortServerResults = new HashMap<>();
    private final Map<Long, List<PerformanceTimer>> receivedSendServerResults = new HashMap<>();

    public PerformanceResults() {}

    public List<Map.Entry<Long, Double>> getClientsStats(int requestsNumber) {
        return getMapMeanStats(clientsResults).stream().map(entry ->
                new AbstractMap.SimpleEntry<>(entry.getKey(), entry.getValue() / requestsNumber)
        ).collect(Collectors.toList());
    }

    public List<Map.Entry<Long, Double>> getSortStats() {
        return getMapMeanStats(sortServerResults);
    }

    public List<Map.Entry<Long, Double>> getReceivedSendStats() {
        return getMapMeanStats(receivedSendServerResults).stream().map(entry ->
            new AbstractMap.SimpleEntry<>(entry.getKey(), entry.getValue())
        ).collect(Collectors.toList());
    }

    private List<Map.Entry<Long, Double>> getMapMeanStats(Map<Long, List<PerformanceTimer>> map) {
        final Map<Long, Double> results = new HashMap<>();
        map.forEach((value, timers) -> {
            double sum = timers.stream().mapToDouble(PerformanceTimer::getDurationSeconds).sum();
            results.put(value, sum / timers.size()); // to mean seconds
        });
        return results.entrySet().stream()
                .sorted(Comparator.comparingLong(Map.Entry::getKey))
                .collect(Collectors.toList());
    }

    public Map<Long, List<PerformanceTimer>> getClientsResults() {
        return clientsResults;
    }

    public Map<Long, List<PerformanceTimer>> getSortServerResults() {
        return sortServerResults;
    }

    public Map<Long, List<PerformanceTimer>> getReceivedSendServerResults() {
        return receivedSendServerResults;
    }

    public void setClientResults(long paramValue, List<PerformanceTimer> results) {
        clientsResults.put(paramValue, results);
    }

    public void setSortServerResults(long paramValue, List<PerformanceTimer> results) {
        sortServerResults.put(paramValue, results);
    }

    public void setReceivedSendServerResults(long paramValue, List<PerformanceTimer> results) {
        receivedSendServerResults.put(paramValue, results);
    }

    public boolean dumpToJSON(Config config, String path, MetricType metricType) {
        ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        JsonNode node = config.toJSON();
        List<Map.Entry<Long, Double>> clientsStats = getClientsStats(config.requestsNumber);
        List<Map.Entry<Long, Double>> sortStats = getSortStats();
        List<Map.Entry<Long, Double>> receivedSendStats = getReceivedSendStats();

        final LinkedList<Long> dynamicValues = new LinkedList<>();
        final LinkedList<Double> clientTime = new LinkedList<>();
        final LinkedList<Double> sortTime = new LinkedList<>();
        final LinkedList<Double> receivedSendTime = new LinkedList<>();

        clientsStats.forEach(entry -> dynamicValues.add(entry.getKey()));
        sortStats.forEach(entry -> sortTime.add(entry.getValue()));
        receivedSendStats.forEach(entry -> receivedSendTime.add(entry.getValue()));
        clientsStats.forEach(entry -> clientTime.add(entry.getValue()));

        ((ObjectNode) node).set("dynamic_param_values", mapper.valueToTree(dynamicValues));

        switch (metricType) {
            case SORT_TIME: {
                ((ObjectNode) node).set("server_sort_time", mapper.valueToTree(sortTime));
                break;
            }
            case SERVER_TIME: {
                ((ObjectNode) node).set("server_request_time", mapper.valueToTree(receivedSendTime));
                break;
            }
            case CLIENT_TIME: {
                ((ObjectNode) node).set("client_request_time", mapper.valueToTree(clientTime));
                break;
            }
            default: {
                ((ObjectNode) node).set("server_sort_time", mapper.valueToTree(sortTime));
                ((ObjectNode) node).set("server_request_time", mapper.valueToTree(receivedSendTime));
                ((ObjectNode) node).set("client_request_time", mapper.valueToTree(clientTime));
                break;
            }
        }
        try {
            mapper.writeValue(new File(path), node);
        } catch (IOException e) {
            return false;
        }
        return true;
    }
}
