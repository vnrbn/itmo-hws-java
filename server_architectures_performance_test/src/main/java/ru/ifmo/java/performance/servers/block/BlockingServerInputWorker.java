package ru.ifmo.java.performance.servers.block;

import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class BlockingServerInputWorker implements Runnable {
    private final Socket socket;
    private final InputStream input;

    private final Lock taskLock;
    private final Condition readyCond;
    private final ExecutorService taskPool;
    private final Queue<BlockingSortTask> taskQueue;
    private final Queue<PerformanceTimer> sortTimers;
    private final Queue<PerformanceTimer> receiveSendTimers;

    private volatile boolean isInterrupted = false;

    public BlockingServerInputWorker(
            Socket socket,
            Queue<PerformanceTimer> receiveSendTimers,
            Queue<PerformanceTimer> sortTimers,
            ExecutorService taskPool,
            Queue<BlockingSortTask> taskQueue,
            Lock taskLock,
            Condition readyCond
    ) throws IOException {
        this.socket = socket;
        this.input = socket.getInputStream();
        this.receiveSendTimers = receiveSendTimers;
        this.sortTimers = sortTimers;
        this.taskPool = taskPool;
        this.taskLock = taskLock;
        this.taskQueue = taskQueue;
        this.readyCond = readyCond;
    }

    @Override
    public void run() {
        try {
            while (!socket.isClosed() && !Thread.currentThread().isInterrupted()) {
                Protocol.SortRequestResponse request = receiveRequest();

                // when thread is interrupted after test -> NPE
                if (request == null) {
                    continue;
                }

                // request received - timer start
                PerformanceTimer receiveSendTimer = new PerformanceTimer();
                receiveSendTimer.start();
                receiveSendTimers.add(receiveSendTimer);

                ArrayList<Integer> values = new ArrayList<>(request.getValuesList());

                BlockingSortTask task = new BlockingSortTask(
                    receiveSendTimer, sortTimers, taskQueue, taskLock, readyCond, values
                );
                taskPool.submit(task);
            }
        } catch (SocketException e) {
            System.err.println("SERVER: INPUT WORKER -- SOCKET IS DEAD");
            // e.printStackTrace();
        } catch (IOException e) {
            System.err.println("SERVER: INPUT WORKER -- IO EXCEPTION");
             e.printStackTrace();
        }
    }

    private Protocol.SortRequestResponse receiveRequest() throws IOException {
        byte[] messageSize = new byte[4];
        input.read(messageSize);
        int size = ByteBuffer.wrap(messageSize).getInt();

        if (size == 0) {
            return null;
        }

        byte[] data = new byte[size];
        int read = 0;
        while (read != size) {
            read += input.read(data, read, size - read);
        }

        return Protocol.SortRequestResponse.parseFrom(data);
    }
}
