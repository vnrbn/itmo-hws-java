package ru.ifmo.java.performance.config;

public enum MetricType {
    SORT_TIME,
    SERVER_TIME,
    CLIENT_TIME,
    ALL
}
