package ru.ifmo.java.performance.client;

import ru.ifmo.java.performance.protocol.Protocol.SortRequestResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public interface SortRequester {
    default SortRequestResponse makeRequestResponse(
            InputStream input,
            OutputStream output,
            SortRequestResponse request
    ) throws IOException {
        sendRequest(output, request);
        return SortRequestResponse.parseDelimitedFrom(input);
    }

    default void sendRequest(OutputStream output, SortRequestResponse request) throws IOException {
        request.writeTo(output);
    }

    default SortRequestResponse getRequest(ArrayList<Integer> data) {
        return SortRequestResponse.newBuilder()
                .addAllValues(data)
                .build();
    }
}
