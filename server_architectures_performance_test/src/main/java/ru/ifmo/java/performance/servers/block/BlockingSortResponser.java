package ru.ifmo.java.performance.servers.block;

import ru.ifmo.java.performance.protocol.Protocol;

import java.util.List;

public interface BlockingSortResponser {
    default Protocol.SortRequestResponse getResponse(List<Integer> data) {
        return Protocol.SortRequestResponse.newBuilder()
                .addAllValues(data)
                .build();
    }
}
