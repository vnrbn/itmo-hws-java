package ru.ifmo.java.performance.servers.nonblock;

import ru.ifmo.java.performance.utils.BubbleSort;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.nio.channels.Selector;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

public class NonBlockingSortTask implements Runnable {
    private static AtomicInteger taskId = new AtomicInteger(0);
    private final int id = taskId.incrementAndGet();

    private final Selector outSelector;
    private final ClientInfo clientInfo;
    private final List<Integer> values;
    private final Queue<PerformanceTimer> sortTimers;
    private final PerformanceTimer sortTimer = new PerformanceTimer();

    public NonBlockingSortTask(
        Selector outSelector,
        ClientInfo clientInfo,
        Queue<PerformanceTimer> sortTimers,
        List<Integer> values
    ) {
        this.outSelector = outSelector;
        this.clientInfo = clientInfo;
        this.sortTimers = sortTimers;
        this.values = values;
    }

    @Override
    public void run() {
//         System.out.println("task " + id + ": running");

        // make job
        sortTimer.start();
        BubbleSort.sort(values);
        sortTimer.stop();

        // register sort time
        sortTimers.add(sortTimer);

        // notify task ready
        clientInfo.addMessage(values);
        outSelector.wakeup();

//         System.out.println("task " + id + ": done");
    }

    public List<Integer> getValues() {
        return values;
    }

    public PerformanceTimer getSortTimer() {
        return sortTimer;
    }

    public static void resetCounter() {
        taskId.set(0);
    }
}
