package ru.ifmo.java.performance.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ru.ifmo.java.performance.protocol.Protocol;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config {
    public final ServerType serverType;
    public final ParameterType dynamicParameterType;

    public volatile String loadFolderPath;
    public volatile int tasksPoolSize;
    public volatile int requestsNumber;
    public volatile ConfigParameter arraySize;
    public volatile ConfigParameter clientsNumber;
    public volatile ConfigParameter requestDeltaTime;

    public Config(ServerType serverType, ParameterType dynamicParameterType) {
        this.loadFolderPath = System.getProperty("user.dir");
        this.tasksPoolSize = 5;
        this.requestsNumber = 1;
        this.serverType = serverType;
        this.dynamicParameterType = dynamicParameterType;
    }

    public Config(int requestsNumber, ServerType serverType, ParameterType dynamicParameterType) {
        this.requestsNumber = requestsNumber;
        this.serverType = serverType;
        this.dynamicParameterType = dynamicParameterType;
    }

    public void setRequestsNumber(int requestsNumber) {
        this.requestsNumber = requestsNumber;
    }

    public void setLoadFolderPath(String loadFolderPath) {
        this.loadFolderPath = loadFolderPath;
    }

    public void setTasksPoolSize(int tasksPoolSize) {
        this.tasksPoolSize = tasksPoolSize;
    }

    public String getLoadFolderPath() {
        return loadFolderPath;
    }

    public int calculateProtoMsgSize() {
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < arraySize.getCurrentValue(); ++i) {
            values.add(i);
        }
        Protocol.SortRequestResponse request = Protocol.SortRequestResponse.newBuilder()
                .addAllValues(values)
                .build();
        return request.toByteArray().length;
    }

    public void setStaticParameter(ParameterType paramType, long value) {
        setParam(paramType, new ConfigParameter(paramType, value));
    }

    public void setDynamicParameter(ParameterType paramType, long beg, long end, long step) {
        setParam(paramType, new ConfigParameter(paramType, beg, end, step));
    }

    public ConfigParameter getParam(ParameterType paramType) {
        switch (paramType) {
            case ARRAY_SIZE:
                return arraySize;
            case CLIENTS_NUMBER:
                return clientsNumber;
            case REQUEST_DELTA_TIME:
                return requestDeltaTime;
            default:
                throw new UnsupportedOperationException("unsupported parameter type: " + paramType);
        }
    }

    public void setParam(ParameterType paramType, ConfigParameter parameter) {
        switch (paramType) {
            case ARRAY_SIZE:
                arraySize = parameter;
                break;
            case CLIENTS_NUMBER:
                clientsNumber = parameter;
                break;
            case REQUEST_DELTA_TIME:
                requestDeltaTime = parameter;
                break;
            default:
                throw new UnsupportedOperationException("unsupported parameter type: " + paramType);
        }
    }

    public JsonNode toJSON() {
        ObjectMapper objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        ObjectNode node = objectMapper.createObjectNode();
        node.put("server", serverType.toString());
        node.put("dynamic_param", dynamicParameterType.toString());
        node.put("requests_number", requestsNumber);
        for (ConfigParameter p: Arrays.asList(arraySize, clientsNumber, requestDeltaTime)) {
            if (p.isDynamic) {
                ObjectNode dpNode = objectMapper.createObjectNode();
                dpNode.put("beg", p.beg);
                dpNode.put("end", p.end);
                dpNode.put("step", p.step);
                node.set(p.type.toString(), dpNode);
            } else {
                node.put(p.type.toString(), p.getCurrentValue());
            }
        }
        return node;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\tserver    : ").append(serverType.toString());
        sb.append("\n");
        sb.append("\tdynamic   : ").append(dynamicParameterType.toString());
        sb.append("\n");
        sb.append("\tclients  #: ").append(clientsNumber.toString());
        sb.append("\n");
        sb.append("\tarray size: ").append(arraySize.toString());
        sb.append("\n");
        sb.append("\tdelta time: ").append(requestDeltaTime.toString());
        sb.append("\n");
        sb.append("\trequests number: ").append(requestsNumber);
        sb.append("\n");
        sb.append("\ttasks pool size: ").append(tasksPoolSize);
        sb.append("\n");
        sb.append("\tprotobuf message size: ").append(calculateProtoMsgSize());
        sb.append("\n");
        sb.append("\tdump folder: ").append(loadFolderPath);
        return sb.toString();
    }
}
