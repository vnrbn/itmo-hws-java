package ru.ifmo.java.performance.servers.block;

import ru.ifmo.java.performance.protocol.Protocol;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Queue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class BlockingServerOutputWorker implements Runnable, BlockingSortResponser {
    private final long WAKE_UP_SECONDS = 1;

    private final Socket socket;
    private final OutputStream output;

    private final Lock taskLock;
    private final Condition readyCond;
    private final Queue<BlockingSortTask> taskQueue;

    private volatile boolean isInterrupted = false;

    public BlockingServerOutputWorker(
            Socket socket,
            Queue<BlockingSortTask> taskQueue,
            Lock taskLock,
            Condition readyCond
    ) throws IOException {
        this.socket = socket;
        this.output = socket.getOutputStream();
        this.taskQueue = taskQueue;
        this.taskLock = taskLock;
        this.readyCond = readyCond;
    }

    @Override
    public void run() {
        while (!socket.isClosed() && !isInterrupted) {
            try {
                taskLock.lockInterruptibly();
                while (!socket.isClosed() && taskQueue.isEmpty() && !isInterrupted) {
                    readyCond.await(WAKE_UP_SECONDS, TimeUnit.SECONDS);
                }
                while (!taskQueue.isEmpty() && !socket.isClosed()) {
                    BlockingSortTask task = taskQueue.poll();
                    if (task != null && !socket.isClosed()) {
                        sendResponse(getResponse(task.getValues()));
                        // client request processed and send - timer stop
                        task.getReceiveSendTimer().stop();
                    }
                }
            } catch (IOException e) {
                System.err.println("SERVER: OUTPUT WORKER -- IO EXCEPTION");
                // e.printStackTrace();
            } catch (InterruptedException e) {
                interrupt();
            } finally {
                taskLock.unlock();
            }
        }
    }

    private void sendResponse(Protocol.SortRequestResponse response) throws IOException {
        byte[] responseBytes = response.toByteArray();

        ByteBuffer responseSize = ByteBuffer.allocate(4);
        responseSize.putInt(responseBytes.length);
        responseSize.flip();

        output.write(responseSize.array());
        output.write(responseBytes);
        output.flush();
    }

    public void interrupt() {
        isInterrupted = true;
        if (!socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }
}
