package ru.ifmo.java.performance.servers.async;

import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

public class AsyncClientInfo {
    private final ByteBuffer sizeBuffer;
    private ByteBuffer inputBuffer;

    private final Queue<PerformanceTimer> sortTimers;

    private final Queue<PerformanceTimer> globalReceiveSendTimers;
    private final ConcurrentLinkedQueue<PerformanceTimer> myReceiveSendTimers = new ConcurrentLinkedQueue<>();
    private final ExecutorService tasksPool;

    private final AsynchronousSocketChannel channel;

    private boolean isDataInput = false;

    public AsyncClientInfo(
            AsynchronousSocketChannel channel,
            Queue<PerformanceTimer> sortTimers,
            Queue<PerformanceTimer> receivedSendTimers,
            ExecutorService tasksPool
    ) {
        this.channel = channel;
        this.sortTimers = sortTimers;
        this.globalReceiveSendTimers = receivedSendTimers;
        this.tasksPool = tasksPool;
        this.sizeBuffer = ByteBuffer.allocate(4);
    }

    public ExecutorService getTasksPool() {
        return tasksPool;
    }

    public Queue<PerformanceTimer> getSortTimers() {
        return sortTimers;
    }

    public Queue<PerformanceTimer> getGlobalReceiveSendTimers() {
        return globalReceiveSendTimers;
    }

    public AsynchronousSocketChannel getChannel() {
        return channel;
    }

    public ByteBuffer getSizeBuffer() {
        return sizeBuffer;
    }

    public ByteBuffer getInputBuffer() {
        return inputBuffer;
    }

    public void setDataInput(boolean value) {
        isDataInput = value;
    }

    public boolean isDataInput() {
        return isDataInput;
    }

    public void setInputBuffer(ByteBuffer buffer) {
        inputBuffer = buffer;
    }

    public Protocol.SortRequestResponse parseMessage() {
        inputBuffer.flip();
        Protocol.SortRequestResponse message = null;
        try {
            message = Protocol.SortRequestResponse.parseFrom(inputBuffer.array());
        } catch (IOException e) {
            e.printStackTrace();
        }
        inputBuffer.clear();
        return message;
    }

    public void writeMessage(List<Integer> values) {
        Protocol.SortRequestResponse message = Protocol.SortRequestResponse.newBuilder()
                .addAllValues(values)
                .build();

        byte[] data = message.toByteArray();

        ByteBuffer sizeBuffer = ByteBuffer.allocate(4);
        sizeBuffer.putInt(data.length);
        sizeBuffer.flip();

        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + data.length);
        byteBuffer.put(sizeBuffer);
        byteBuffer.put(data);
        byteBuffer.flip();

        channel.write(byteBuffer, this, new AsyncWriteHandler(byteBuffer));
    }

    public Queue<PerformanceTimer> getMyReceiveSendTimers() {
        return myReceiveSendTimers;
    }

    public void addNewTimer(PerformanceTimer performanceTimer) {
        myReceiveSendTimers.add(performanceTimer);
    }
}