package ru.ifmo.java.performance.utils;

import java.util.ArrayList;
import java.util.List;

public class BubbleSort {
    public static void sort(List<Integer> values) {
        if (values.size() == 0) return;
        int max_idx, tmp;
        for (int i = 0; i < values.size(); ++i) {
            max_idx = 0;
            for (int j = 1; j < values.size() - i; ++j) {
                if (values.get(j) > values.get(max_idx)) {
                    max_idx = j;
                }
            }
            tmp = values.get(values.size() - 1 - i);
            values.set(values.size() - 1 - i, values.get(max_idx));
            values.set(max_idx, tmp);
        }
    }

    public static List<Integer> valuesToArrayList(List<Integer> list) {
        return new ArrayList<>(list);
    }
}
