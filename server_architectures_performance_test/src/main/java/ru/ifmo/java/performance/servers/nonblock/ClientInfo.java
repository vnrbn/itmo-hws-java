package ru.ifmo.java.performance.servers.nonblock;

import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ClientInfo {
    private final ByteBuffer sizeBuffer;
    private ByteBuffer inputBuffer;
    private final ConcurrentLinkedQueue<ByteBuffer> outputBuffers = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<PerformanceTimer> receivedSendTimers = new ConcurrentLinkedQueue<>();

    private final SocketChannel channel;
    private SelectionKey outKey = null;

    private boolean isDataInput = false;
    private boolean isRegistered = false;

    public ClientInfo(SocketChannel channel) {
        this.channel = channel;
        this.sizeBuffer = ByteBuffer.allocate(4);
    }

    public void unregister() {
        outKey.cancel();
        outKey = null;
        isRegistered = false;
    }

    public ByteBuffer getSizeBuffer() {
        return sizeBuffer;
    }

    public SocketChannel getChannel() {
        return channel;
    }

    public void setOutKey(SelectionKey outKey) {
        this.outKey = outKey;
    }

    public void setRegistered(boolean registered) {
        isRegistered = registered;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public SelectionKey getOutKey() {
        return outKey;
    }

    public ByteBuffer getInputBuffer() {
        return inputBuffer;
    }

    public void setDataInput(boolean value) {
        isDataInput = value;
    }

    public boolean isDataInput() {
        return isDataInput;
    }

    public void setInputBuffer(ByteBuffer buffer) {
        inputBuffer = buffer;
    }

    public ConcurrentLinkedQueue<ByteBuffer> getOutputBuffers() {
        return outputBuffers;
    }

    public Protocol.SortRequestResponse parseMessage() {
        inputBuffer.flip();
        Protocol.SortRequestResponse message = null;
        try {
            message = Protocol.SortRequestResponse.parseFrom(inputBuffer.array());
        } catch (IOException e) {
            e.printStackTrace();
        }
        inputBuffer.clear();
        return message;
    }

    public void addMessage(List<Integer> values) {
        Protocol.SortRequestResponse message = Protocol.SortRequestResponse.newBuilder()
                .addAllValues(values)
                .build();

        byte[] data = message.toByteArray();

        ByteBuffer sizeBuffer = ByteBuffer.allocate(4);
        sizeBuffer.putInt(data.length);
        sizeBuffer.flip();

        ByteBuffer byteBuffer = ByteBuffer.allocate(4 + data.length);
        byteBuffer.put(sizeBuffer);
        byteBuffer.put(data);
        byteBuffer.flip();

        outputBuffers.add(byteBuffer);
    }

    public Queue<PerformanceTimer> getReceivedSendTimers() {
        return receivedSendTimers;
    }

    public void addNewTimer(PerformanceTimer performanceTimer) {
        receivedSendTimers.add(performanceTimer);
    }
}
