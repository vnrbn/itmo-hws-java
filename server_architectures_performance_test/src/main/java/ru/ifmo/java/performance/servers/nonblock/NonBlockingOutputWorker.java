package ru.ifmo.java.performance.servers.nonblock;

import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

public class NonBlockingOutputWorker implements Runnable {
    private final Selector selector;
    private final Queue<PerformanceTimer> receivedSendTimers;
    private final CopyOnWriteArrayList<ClientInfo> clients = new CopyOnWriteArrayList<>();

    private volatile boolean isInterrupted = false;

    public NonBlockingOutputWorker(Selector selector, Queue<PerformanceTimer> receivedSendTimers) {
        this.selector = selector;
        this.receivedSendTimers = receivedSendTimers;
    }

    @Override
    public void run() {
        System.out.println("OUT - SELECTOR READY");
        try {
            while (selector.isOpen() && !isInterrupted) {
                checkClientsToRegister();
                if (selector.select(1000) == 0) {
                    continue;
                }
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    if (!key.channel().isOpen()) {
                        key.cancel();
                        iterator.remove();
                        continue;
                    }
                    if (key.isWritable()) {
                        processChannelWrite(key);
                    }
                    iterator.remove();
                }
            }
        } catch (IOException e) {
            System.out.println("SERVER: INPUT SELECTOR IO EXCEPTION");
            // e.printStackTrace();
        }
    }

    private void checkClientsToRegister() throws IOException {
        for (ClientInfo info : clients) {
            if (info.getOutputBuffers().isEmpty()) {
                if (info.isRegistered()) {
                    info.unregister();
                }
            } else {
                if (!info.isRegistered()) {
                    registerClient(info);
                    info.setRegistered(true);
                }
            }
        }
    }

    private void registerClient(ClientInfo info) throws IOException {
        SelectionKey outputKey = info.getChannel().register(selector, SelectionKey.OP_WRITE);
        outputKey.attach(info);
        info.setOutKey(outputKey);
    }

    private void processChannelWrite(SelectionKey key) {
        SocketChannel channel = (SocketChannel) key.channel();
        ClientInfo clientInfo = (ClientInfo) key.attachment();
        ConcurrentLinkedQueue<ByteBuffer> outputBuffers = clientInfo.getOutputBuffers();
        Iterator<ByteBuffer> iterator = outputBuffers.iterator();
        if (iterator.hasNext()) {
            ByteBuffer outputBuffer = iterator.next();
            if (outputBuffer.hasRemaining()) {
                try {
                    int bytesWritten = channel.write(outputBuffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (!outputBuffer.hasRemaining()) {
                // client request processed and send - timer stop
                PerformanceTimer receivedSendTimer = clientInfo.getReceivedSendTimers().poll();
                if (receivedSendTimer != null) {
                    receivedSendTimer.stop();
                    receivedSendTimers.add(receivedSendTimer);
                }
                iterator.remove();
            }
        }
    }

    public List<ClientInfo> getClients() {
        return clients;
    }

    public void interrupt() {
        isInterrupted = true;
    }
}
