package ru.ifmo.java.performance.config;

import java.util.concurrent.atomic.AtomicLong;

public class ConfigParameter {
    public final long beg;
    public final long end;
    public final long step;
    public final AtomicLong currValue;
    public final boolean isDynamic;
    public boolean isFirst = true;
    public ParameterType type;

    public ConfigParameter(ParameterType type, long value) {
        this.type = type;
        this.beg = value;
        this.end = value;
        this.step = 0;
        this.currValue = new AtomicLong(value);
        this.isDynamic = false;
        this.isFirst = false;
    }

    public ConfigParameter(ParameterType type, long beg, long end, long step) {
        this.type = type;
        this.beg = beg;
        this.end = end;
        this.step = step;
        this.currValue = new AtomicLong(beg);
        this.isDynamic = true;
    }

    public boolean hasNext() {
        if (isFirst) {
            return true;
        }
        return isDynamic && (currValue.get() + step <= end);
    }

    public long incrementAndGet() {
        if (isFirst) {
            isFirst = false;
            return currValue.get();
        }
        if (hasNext()) {
            return currValue.addAndGet(step);
        }
        return currValue.get();
    }

    public long getCurrentValue() {
        return currValue.get();
    }

    @Override
    public String toString() {
        if (isDynamic) {
            return String.format("[beg: %d, end: %d, step: %d, curr: %d]", beg, end, step, currValue.get());
        }
        return String.format("[curr: %d]", currValue.get());
    }
}
