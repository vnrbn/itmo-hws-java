package ru.ifmo.java.performance.tester;

import org.jfree.chart.JFreeChart;
import ru.ifmo.java.performance.client.Client;
import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.config.ConfigParameter;
import ru.ifmo.java.performance.config.MetricType;
import ru.ifmo.java.performance.graphics.PlotBuilder;
import ru.ifmo.java.performance.gui.GuiTesterProvider;
import ru.ifmo.java.performance.servers.Server;
import ru.ifmo.java.performance.servers.ServerFactory;
import ru.ifmo.java.performance.servers.block.BlockingSortTask;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PerformanceTester implements Runnable {
    private Config config;

    private ExecutorService pool;
    private CountDownLatch startLatch;
    private CountDownLatch stopLatch;

    private Server server;
    private List<Client> clients = new LinkedList<>();

    private PerformanceResults results = new PerformanceResults();
    private final GuiTesterProvider guiTesterProvider;

    public PerformanceTester(GuiTesterProvider guiTesterProvider) {
        this.guiTesterProvider = guiTesterProvider;
    }

    @Override
    public void run() {
        awaitGui();
        ConfigParameter dynamicParameter = config.getParam(config.dynamicParameterType);
        while (dynamicParameter.hasNext()) {
            // update params
            dynamicParameter.incrementAndGet();
            printConfig();

            // run
            runTest();
            awaitTestReady();

            // save and clean to next test
            server.shutDown();
            saveResults();
            clearAll();
            System.out.println("------------TEST ENDED------------\n\n");
            guiTesterProvider.notifyTesterEvent();
        }
        printResults();
        dumpResults();
        guiTesterProvider.setTestReady(true);
        guiTesterProvider.notifyTesterEvent();
    }

    private void awaitGui() {
        try {
            guiTesterProvider.getGuiLock().lockInterruptibly();
            while (!guiTesterProvider.isGuiReady()) {
                guiTesterProvider.getGuiEvent().await(5, TimeUnit.SECONDS);
            }
            config = guiTesterProvider.config();
        } catch (InterruptedException e) {
            // ignore
        } finally {
            guiTesterProvider.getGuiLock().unlock();
        }
    }

    private void saveResults() {
        long currValue = config.getParam(config.dynamicParameterType).getCurrentValue();

        results.setSortServerResults(currValue, new ArrayList<>(server.getSortTimers()));
        results.setReceivedSendServerResults(currValue, new ArrayList<>(server.getReceiveSendTimers()));
        results.setClientResults(
                currValue,
                clients.stream()
                        .map(Client::getTimer)
                        .collect(Collectors.toList())
        );
    }

    private void dumpResults() {
        File plotsDir = new File(config.loadFolderPath);
        File dataDir = new File(plotsDir.getAbsolutePath() + File.separator + "data");
        if (!plotsDir.exists()) {
            plotsDir.mkdirs();
        }
        if (!dataDir.exists()) {
            dataDir.mkdirs();
        }

        MetricType[] metricType = MetricType.values();
        List<JFreeChart> plots = PlotBuilder.buildPlotForEveryMetric(config, results);
        for (int i = 0; i < 3 && i < plots.size(); ++i) {
            String graphPath = plotsDir.getAbsolutePath() + File.separator +
                    config.serverType.toString().toLowerCase() + "_" + (i + 1);
            String dataPath = dataDir.getAbsolutePath() + File.separator +
                    config.serverType.toString().toLowerCase() + "_" + (i + 1) + ".json";
            PlotBuilder.dumpAsPNG(graphPath, plots.get(i), 1000,1000);
            results.dumpToJSON(config, dataPath, metricType[i]);
        }
        if (plots.size() == 4) {
            String path = plotsDir.getAbsolutePath() + File.separator +
                    config.serverType.toString().toLowerCase() + "_summary";
            String dataPath = dataDir.getAbsolutePath() + File.separator +
                    config.serverType.toString().toLowerCase() + "_summary.json";
            PlotBuilder.dumpAsPNG(path, plots.get(3),1000,1000);
            results.dumpToJSON(config, dataPath, MetricType.ALL);
        }
    }

    private void printResults() {
        System.out.println("-------client results---------");
        results.getClientsStats(config.requestsNumber).forEach(entry ->
            System.out.println("\tvalue: " + entry.getKey() + "\ttime: " + String.format("%.2f", entry.getValue()))
        );
        System.out.println("\n-------sort results---------");
        results.getSortStats().forEach(entry ->
            System.out.println("\tvalue: " + entry.getKey() + "\ttime: " + String.format("%.2f", entry.getValue()))
        );
        System.out.println("\n-------received-send results---------");
        results.getReceivedSendStats().forEach(entry ->
            System.out.println("\tvalue: " + entry.getKey() + "\ttime: " + String.format("%.2f", entry.getValue()))
        );
    }

    private void runTest() {
        initClientsServerPool();
        initLatches();
        initClients();
        initServer();

        pool.submit(server);
        clients.forEach(client -> pool.submit(client));
    }

    private void awaitTestReady() {
        try {
            stopLatch.await(); // spurious wakeup's don't affect
        } catch (InterruptedException e) {
            System.err.println("PERFORMANCE TESTER: THREAD WAS INTERRUPTED");
        }
    }

    private void initClientsServerPool() {
        pool = Executors.newFixedThreadPool((int) this.config.clientsNumber.getCurrentValue() + 1);
    }

    private void initLatches() {
        startLatch = new CountDownLatch((int) (config.clientsNumber.getCurrentValue() + 1));
        stopLatch = new CountDownLatch((int) (config.clientsNumber.getCurrentValue()));
    }

    private void initClients() {
        Client.resetCounter();
        for (int i = 0; i < config.clientsNumber.getCurrentValue(); ++i) {
            clients.add(new Client(config, startLatch, stopLatch));
        }
    }

    private void initServer() {
        BlockingSortTask.resetCounter();
        this.server = ServerFactory.newServerWithStartLatch(config.serverType, startLatch, config.tasksPoolSize);
    }

    private void clearAll() {
        clients.clear();
        pool.shutdownNow();

        pool = null;
        startLatch = null;
        stopLatch = null;
    }

    private void printConfig() {
        System.out.println("------------TEST STARTED------------");
        System.out.println(config.toString());
        System.out.println("------------------------------------");
    }
}
