package ru.ifmo.java.performance.servers.async;

import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.nio.ByteBuffer;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.List;

public class AsyncReadHandler implements CompletionHandler<Integer, AsyncClientInfo> {
    @Override
    public void completed(Integer result, AsyncClientInfo clientInfo) {
        if (!clientInfo.getChannel().isOpen()) {
            return;
        }
        if (!clientInfo.isDataInput()) {
            if (result <= 0) {
                return;
            }
            if (clientInfo.getSizeBuffer().hasRemaining()) {
                clientInfo.getChannel().read(clientInfo.getSizeBuffer(), clientInfo, this);
            } else {
                clientInfo.getSizeBuffer().flip();
                clientInfo.setInputBuffer(ByteBuffer.allocate(clientInfo.getSizeBuffer().getInt()));
                clientInfo.setDataInput(true);

                clientInfo.getChannel().read(clientInfo.getInputBuffer(), clientInfo, this);
            }
        } else {
            if (clientInfo.getInputBuffer().hasRemaining()) {
                clientInfo.getChannel().read(clientInfo.getInputBuffer(), clientInfo, this);
            } else {
                Protocol.SortRequestResponse request = clientInfo.parseMessage();
                if (request == null) {
                    System.err.println("SERVER: READ HANDLER - REQUEST IS NULL");
                    return;
                }
                PerformanceTimer receivedSendTimer = new PerformanceTimer();
                receivedSendTimer.start();
                clientInfo.addNewTimer(receivedSendTimer);

                List<Integer> values = new ArrayList<>(request.getValuesList());
                AsyncSortTask task = new AsyncSortTask(clientInfo, values);
                clientInfo.getTasksPool().submit(task);

                clientInfo.setDataInput(false);
                clientInfo.getSizeBuffer().clear();
                clientInfo.setInputBuffer(null);

                clientInfo.getChannel().read(clientInfo.getSizeBuffer(), clientInfo, this);
            }
        }
    }

    @Override
    public void failed(Throwable exc, AsyncClientInfo clientInfo) {
        System.err.println("SERVER: READ HANDLER -- EXCEPTION");
        exc.printStackTrace();
    }
}
