package ru.ifmo.java.performance.servers.block;

import ru.ifmo.java.performance.servers.Server;
import ru.ifmo.java.performance.utils.Constants;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BlockingServer implements Server {
    private final List<ExecutorService> outputThreads = new LinkedList<>();
    private final List<Thread> inputThreads = new LinkedList<>();
    private final ExecutorService tasksPool;

    private final ConcurrentLinkedQueue<PerformanceTimer> sortTimers = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<PerformanceTimer> receiveSendTimers = new ConcurrentLinkedQueue<>();

    private ServerSocket socket;
    private final CountDownLatch startLatch;
    private volatile boolean isInterrupted = false;

    public BlockingServer(CountDownLatch latch, int tasksPoolSize) {
        tasksPool = Executors.newFixedThreadPool(tasksPoolSize);
        this.startLatch = latch;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(Constants.SERVER_PORT)) {
            System.out.println("SERVER READY");

            this.socket = serverSocket;
            // server ready
            startLatch.countDown();

            while (!isInterrupted && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                processClient(socket);
            }
        } catch (SocketException e) {
            // System.err.println("SERVER SOCKET IS DEAD");
            // e.printStackTrace();
        } catch (IOException e) {
            System.err.println("SERVER IO EXCEPTION");
            e.printStackTrace();
        }
        System.out.println("[X] - SERVER DONE");
    }

    private void processClient(Socket socket) {
        System.out.println("SERVER: client connected " + socket.getRemoteSocketAddress().toString());
        try {
            // tasks sync instruments
            Lock taskLock = new ReentrantLock();
            Condition readyCond = taskLock.newCondition();
            ConcurrentLinkedQueue<BlockingSortTask> taskQueue = new ConcurrentLinkedQueue<>();

            // worker for client requests
            BlockingServerInputWorker inputWorker = new BlockingServerInputWorker(
                    socket, receiveSendTimers, sortTimers, tasksPool, taskQueue, taskLock, readyCond
            );
            Thread thread = new Thread(inputWorker);
            thread.start();
            inputThreads.add(thread);

            // worker for client responses
            ExecutorService outputThread = Executors.newSingleThreadExecutor();
            outputThreads.add(outputThread);
            BlockingServerOutputWorker outputWorker = new BlockingServerOutputWorker(
                    socket, taskQueue, taskLock, readyCond
            );
            outputThread.submit(outputWorker);
        } catch (IOException e) {
            System.err.println("SERVER: CLIENT CONNECTION PROCESS FAILED");
            e.printStackTrace();
        }
    }

    @Override
    public void shutDown() {
        isInterrupted = true;
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                // ignore
            }
        }
        outputThreads.forEach(ExecutorService::shutdownNow);
        inputThreads.forEach(Thread::interrupt);
        tasksPool.shutdownNow();

        outputThreads.clear();
        inputThreads.clear();
    }

    @Override
    public Queue<PerformanceTimer> getSortTimers() {
        return sortTimers;
    }

    @Override
    public Queue<PerformanceTimer> getReceiveSendTimers() {
        return receiveSendTimers;
    }
}
