package ru.ifmo.java.performance;

import ru.ifmo.java.performance.config.Config;
import ru.ifmo.java.performance.config.ParameterType;
import ru.ifmo.java.performance.config.ServerType;
import ru.ifmo.java.performance.gui.GuiTesterProvider;
import ru.ifmo.java.performance.gui.PerformanceGUI;
import ru.ifmo.java.performance.tester.PerformanceTester;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void runClientsTest() {
        for (ServerType type : ServerType.values()) {
            Config config = new Config(type, ParameterType.CLIENTS_NUMBER);
            config.setLoadFolderPath("plots_clients");
            config.setTasksPoolSize(5);
            config.setRequestsNumber(5);

            config.setDynamicParameter(ParameterType.CLIENTS_NUMBER, 10, 80, 10);
            config.setStaticParameter(ParameterType.REQUEST_DELTA_TIME, 1000);
            config.setStaticParameter(ParameterType.ARRAY_SIZE, 40000);

            PerformanceTester tester = new PerformanceTester(new GuiTesterProvider(config));
            tester.run();
        }
    }

    public static void runTimeTest() {
        for (ServerType type : ServerType.values()) {
            Config config = new Config(type, ParameterType.REQUEST_DELTA_TIME);
            config.setLoadFolderPath("plots_time");
            config.setTasksPoolSize(5);
            config.setRequestsNumber(5);

            config.setStaticParameter(ParameterType.CLIENTS_NUMBER, 10);
            config.setDynamicParameter(ParameterType.REQUEST_DELTA_TIME, 1000, 10000, 1000);
            config.setStaticParameter(ParameterType.ARRAY_SIZE, 40000);

            PerformanceTester tester = new PerformanceTester(new GuiTesterProvider(config));
            tester.run();
        }
    }

    public static void runSizeTest() {
        for (ServerType type : ServerType.values()) {
            Config config = new Config(type, ParameterType.ARRAY_SIZE);
            config.setLoadFolderPath("plots_size");
            config.setTasksPoolSize(5);
            config.setRequestsNumber(5);

            config.setStaticParameter(ParameterType.CLIENTS_NUMBER, 10);
            config.setStaticParameter(ParameterType.REQUEST_DELTA_TIME, 1000);
            config.setDynamicParameter(ParameterType.ARRAY_SIZE, 10000, 80000, 10000);

            PerformanceTester tester = new PerformanceTester(new GuiTesterProvider(config));
            tester.run();
        }
    }

    public static void runAllTests() {
        runClientsTest();
        runTimeTest();
        runSizeTest();
    }

    public static void runWithoutGui() {
        Config config = new Config(ServerType.ASYNC, ParameterType.CLIENTS_NUMBER);
        config.setLoadFolderPath("plots");
        config.setTasksPoolSize(5);
        config.setRequestsNumber(5);

        config.setDynamicParameter(ParameterType.CLIENTS_NUMBER, 1, 10, 1);
        config.setStaticParameter(ParameterType.REQUEST_DELTA_TIME, 1000);
        config.setStaticParameter(ParameterType.ARRAY_SIZE, 40000);

        PerformanceTester tester = new PerformanceTester(new GuiTesterProvider(config));
        tester.run();
    }

    public static void runWithGui() {
        GuiTesterProvider guiTesterProvider = new GuiTesterProvider();
        ExecutorService pool = Executors.newFixedThreadPool(2);
        pool.submit(new PerformanceGUI(guiTesterProvider));
        pool.submit(new PerformanceTester(guiTesterProvider));
    }

    public static void main(String[] args) {
        runWithGui();
    }
}
