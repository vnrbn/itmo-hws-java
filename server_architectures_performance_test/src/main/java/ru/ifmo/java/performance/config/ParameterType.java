package ru.ifmo.java.performance.config;

public enum ParameterType {
    ARRAY_SIZE,
    CLIENTS_NUMBER,
    REQUEST_DELTA_TIME;

    public static ParameterType getByToString(String str) {
        switch (str) {
            case "clients_number": return CLIENTS_NUMBER;
            case "array_size": return ARRAY_SIZE;
            case "requests_delta_time": return REQUEST_DELTA_TIME;
            default: return null;
        }
    }

    @Override
    public String toString() {
        switch (this) {
            case CLIENTS_NUMBER: return "clients_number";
            case ARRAY_SIZE: return "array_size";
            case REQUEST_DELTA_TIME: return "requests_delta_time";
            default: throw new IllegalArgumentException();
        }
    }
}
