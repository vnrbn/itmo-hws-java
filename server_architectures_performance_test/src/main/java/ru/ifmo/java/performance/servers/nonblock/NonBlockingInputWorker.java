package ru.ifmo.java.performance.servers.nonblock;

import ru.ifmo.java.performance.protocol.Protocol;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;

public class NonBlockingInputWorker implements Runnable {
    private final Selector outSelector;
    private final Selector inSelector;
    private final ExecutorService tasksPool;
    private final Queue<PerformanceTimer> sortTimers;
    private final ConcurrentLinkedQueue<ClientInfo> clientsToRegister = new ConcurrentLinkedQueue<>();

    private volatile boolean isInterrupted = false;

    public NonBlockingInputWorker(
            Selector outSelector,
            Selector inSelector,
            ExecutorService tasksPool,
            Queue<PerformanceTimer> sortTimers
    ) {
        this.outSelector = outSelector;
        this.inSelector = inSelector;
        this.tasksPool = tasksPool;
        this.sortTimers = sortTimers;
    }

    @Override
    public void run() {
        System.out.println("IN - SELECTOR READY");
        try {
            while (inSelector.isOpen() && !isInterrupted) {
                while (!clientsToRegister.isEmpty()) {
                    ClientInfo info = clientsToRegister.poll();
                    registerClient(info);
                }
                if (inSelector.select(1000) == 0) {
                    continue;
                }
                Set<SelectionKey> keys = inSelector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    if (key.isReadable()) {
                        processChannelRead(key);
                    }
                    iterator.remove();
                }
            }
        } catch (ClosedChannelException e) {
            // ignore
        } catch (IOException e) {
            System.out.println("SERVER: INPUT SELECTOR IO EXCEPTION");
            // e.printStackTrace();
        }
    }

    private void registerClient(ClientInfo info) throws IOException {
        SelectionKey inputKey = info.getChannel().register(inSelector, SelectionKey.OP_READ);
        inputKey.attach(info);
    }

    private void processChannelRead(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ClientInfo clientInfo = (ClientInfo) key.attachment();
        // read size buffer
        if (!clientInfo.isDataInput()) {
            int read = channel.read(clientInfo.getSizeBuffer());
            if (read <= 0) {
                return;
            }
            if (!clientInfo.getSizeBuffer().hasRemaining()) {
                clientInfo.getSizeBuffer().flip();
                clientInfo.setInputBuffer(ByteBuffer.allocate(clientInfo.getSizeBuffer().getInt()));
                clientInfo.setDataInput(true);
            }
        } else {
            int read = channel.read(clientInfo.getInputBuffer());
            if (!clientInfo.getInputBuffer().hasRemaining()) {
                Protocol.SortRequestResponse request = clientInfo.parseMessage();
                if (request == null) {
                    System.err.println("SERVER: IN-SELECTOR - REQUEST IS NULL");
                    return;
                }
                PerformanceTimer receivedSendTimer = new PerformanceTimer();
                receivedSendTimer.start();
                clientInfo.addNewTimer(receivedSendTimer);

                List<Integer> values = new ArrayList<>(request.getValuesList());
                NonBlockingSortTask task = new NonBlockingSortTask(outSelector, clientInfo, sortTimers, values);
                tasksPool.submit(task);

                clientInfo.setDataInput(false);
                clientInfo.getSizeBuffer().clear();
                clientInfo.setInputBuffer(null);
            }
        }
    }

    public Queue<ClientInfo> getClientsToRegister() {
        return clientsToRegister;
    }

    public void interrupt() {
        isInterrupted = true;
    }
}
