package ru.ifmo.java.performance.utils;

import java.util.ArrayList;

public class ListFactory {
    public static ArrayList<Integer> genList(int size) {
        ArrayList<Integer> array = new ArrayList<>(size);
        for (int i = 0; i < size; ++i) {
            array.add(size - i);
        }
        return array;
    }
}
