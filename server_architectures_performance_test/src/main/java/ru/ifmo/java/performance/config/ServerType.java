package ru.ifmo.java.performance.config;

public enum ServerType {
    BLOCKING,
    NON_BLOCKING,
    ASYNC;

    public String toString() {
        switch (this) {
            case BLOCKING: return "BLOCKING";
            case NON_BLOCKING: return "NON-BLOCKING";
            case ASYNC: return "ASYNCHRONOUS";
            default: throw new IllegalArgumentException();
        }
    }
}
