package ru.ifmo.java.performance.servers.block;

import ru.ifmo.java.performance.utils.BubbleSort;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.util.List;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class BlockingSortTask implements Runnable {
    private static AtomicInteger taskId = new AtomicInteger(0);
    private final int id = taskId.incrementAndGet();

    private final List<Integer> values;

    private final Lock taskLock;
    private final Condition readyCond;
    private final Queue<BlockingSortTask> taskQueue;
    private final PerformanceTimer sortTimer = new PerformanceTimer();
    private final PerformanceTimer receiveSendTimer;
    private final Queue<PerformanceTimer> sortTimers;

    private volatile boolean isReady = false;

    public BlockingSortTask(
            PerformanceTimer receiveSendTimer,
            Queue<PerformanceTimer> sortTimers,
            Queue<BlockingSortTask> taskQueue,
            Lock taskLock,
            Condition readyCond,
            List<Integer> values
    ) {
        this.receiveSendTimer = receiveSendTimer;
        this.sortTimers = sortTimers;
        this.taskQueue = taskQueue;
        this.taskLock = taskLock;
        this.readyCond = readyCond;
        this.values = values;
    }

    @Override
    public void run() {
        // make job
        sortTimer.start();
        BubbleSort.sort(values);
        sortTimer.stop();

        // register sort time
        sortTimers.add(sortTimer);

        // notify task ready
        taskQueue.add(this);
        isReady = true;
        notifyReady();
    }

    public void notifyReady() {
        try {
            taskLock.lockInterruptibly();
            readyCond.notifyAll();
        } catch (InterruptedException e) {
            // ignore
        } finally {
            taskLock.unlock();
        }
    }

    public List<Integer> getValues() {
        return values;
    }

    public boolean isReady() {
        return isReady;
    }

    public PerformanceTimer getSortTimer() {
        return sortTimer;
    }

    public PerformanceTimer getReceiveSendTimer() {
        return receiveSendTimer;
    }

    public static void resetCounter() {
        taskId.set(0);
    }
}
