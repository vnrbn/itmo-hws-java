package ru.ifmo.java.performance.servers.nonblock;

import ru.ifmo.java.performance.servers.Server;
import ru.ifmo.java.performance.utils.Constants;
import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.*;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NonBlockingServer implements Server {
    private final CountDownLatch latch;

    private final ConcurrentLinkedQueue<PerformanceTimer> sortTimers = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<PerformanceTimer> receiveSendTimers = new ConcurrentLinkedQueue<>();

    private final ExecutorService inputSelectorThread = Executors.newSingleThreadExecutor();
    private final ExecutorService outputSelectorThread = Executors.newSingleThreadExecutor();
    private final ExecutorService tasksPool;

    private final ConcurrentLinkedQueue<SocketChannel> clientsChannels = new ConcurrentLinkedQueue<>();

    private Selector inputSelector;
    private Selector outputSelector;
    private ServerSocketChannel serverSocketChannel;
    private NonBlockingInputWorker inputWorker;
    private NonBlockingOutputWorker outputWorker;

    private volatile boolean isInterrupted = false;

    public NonBlockingServer(CountDownLatch latch, int tasksPoolSize) {
        this.tasksPool = Executors.newFixedThreadPool(tasksPoolSize);
        this.latch = latch;
    }

    @Override
    public void run() {
        if (!openSelectors()) {
            System.out.println("SELECTORS OPEN ERROR");
            return;
        }
        initIOWorkers();
        try(ServerSocketChannel serverSocketChannel = ServerSocketChannel.open()
                .bind(new InetSocketAddress("localhost", Constants.SERVER_PORT))) {
            this.serverSocketChannel = serverSocketChannel;
            System.out.println("SERVER READY");
            // server ready
            latch.countDown();
            while (serverSocketChannel.isOpen() && !isInterrupted) {
                SocketChannel clientChannel = serverSocketChannel.accept();
                System.out.println("new client connected " + clientChannel.socket().getRemoteSocketAddress());
                registerClient(clientChannel);
            }
        } catch (ClosedChannelException e) {
            // ignore
        } catch (IOException e) {
            System.out.println("SERVER: IO EXCEPTION");
            e.printStackTrace();
        }
        System.out.println("[X] - SERVER DONE");
    }

    private void registerClient(SocketChannel clientChannel) throws IOException {
        clientsChannels.add(clientChannel);
        clientChannel.configureBlocking(false);

        ClientInfo info = new ClientInfo(clientChannel);
        inputWorker.getClientsToRegister().add(info);
        outputWorker.getClients().add(info);

        inputSelector.wakeup();
    }

    private void initIOWorkers() {
        inputWorker = new NonBlockingInputWorker(outputSelector, inputSelector, tasksPool, sortTimers);
        outputWorker = new NonBlockingOutputWorker(outputSelector, receiveSendTimers);
        inputSelectorThread.submit(inputWorker);
        outputSelectorThread.submit(outputWorker);
    }

    private boolean openSelectors() {
        try {
            inputSelector = Selector.open();
            outputSelector = Selector.open();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public void shutDown() {
        isInterrupted = true;
        if (serverSocketChannel != null && serverSocketChannel.isOpen()) {
            try {
                serverSocketChannel.close();
            } catch (IOException e) {
                // ignore
            }
        }
        clientsChannels.forEach(channel -> {
            if (channel.isOpen()) {
                try {
                    channel.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        });
        if (inputSelector.isOpen()) {
            try {
                inputSelector.close();
            } catch (IOException e) {
                // ignore
            }
        }
        if (outputSelector.isOpen()) {
            try {
                outputSelector.close();
            } catch (IOException e) {
                // ignore
            }
        }
        inputSelectorThread.shutdownNow();
        outputSelectorThread.shutdownNow();
        tasksPool.shutdownNow();
    }

    @Override
    public Queue<PerformanceTimer> getSortTimers() {
        return sortTimers;
    }

    @Override
    public Queue<PerformanceTimer> getReceiveSendTimers() {
        return receiveSendTimers;
    }
}
