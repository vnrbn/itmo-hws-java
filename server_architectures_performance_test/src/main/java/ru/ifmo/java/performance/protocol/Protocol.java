// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: protocol.proto

package ru.ifmo.java.performance.protocol;

public final class Protocol {
  private Protocol() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface SortRequestResponseOrBuilder extends
      // @@protoc_insertion_point(interface_extends:ru.ifmo.java.performance.protocol.SortRequestResponse)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>repeated int32 values = 1;</code>
     * @return A list containing the values.
     */
    java.util.List<Integer> getValuesList();
    /**
     * <code>repeated int32 values = 1;</code>
     * @return The count of values.
     */
    int getValuesCount();
    /**
     * <code>repeated int32 values = 1;</code>
     * @param index The index of the element to return.
     * @return The values at the given index.
     */
    int getValues(int index);
  }
  /**
   * Protobuf type {@code ru.ifmo.java.performance.protocol.SortRequestResponse}
   */
  public  static final class SortRequestResponse extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:ru.ifmo.java.performance.protocol.SortRequestResponse)
      SortRequestResponseOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use SortRequestResponse.newBuilder() to construct.
    private SortRequestResponse(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private SortRequestResponse() {
      values_ = emptyIntList();
    }

    @Override
    @SuppressWarnings({"unused"})
    protected Object newInstance(
        UnusedPrivateParameter unused) {
      return new SortRequestResponse();
    }

    @Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private SortRequestResponse(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new NullPointerException();
      }
      int mutable_bitField0_ = 0;
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 8: {
              if (!((mutable_bitField0_ & 0x00000001) != 0)) {
                values_ = newIntList();
                mutable_bitField0_ |= 0x00000001;
              }
              values_.addInt(input.readInt32());
              break;
            }
            case 10: {
              int length = input.readRawVarint32();
              int limit = input.pushLimit(length);
              if (!((mutable_bitField0_ & 0x00000001) != 0) && input.getBytesUntilLimit() > 0) {
                values_ = newIntList();
                mutable_bitField0_ |= 0x00000001;
              }
              while (input.getBytesUntilLimit() > 0) {
                values_.addInt(input.readInt32());
              }
              input.popLimit(limit);
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        if (((mutable_bitField0_ & 0x00000001) != 0)) {
          values_.makeImmutable(); // C
        }
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return Protocol.internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor;
    }

    @Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return Protocol.internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              SortRequestResponse.class, Builder.class);
    }

    public static final int VALUES_FIELD_NUMBER = 1;
    private com.google.protobuf.Internal.IntList values_;
    /**
     * <code>repeated int32 values = 1;</code>
     * @return A list containing the values.
     */
    public java.util.List<Integer>
        getValuesList() {
      return values_;
    }
    /**
     * <code>repeated int32 values = 1;</code>
     * @return The count of values.
     */
    public int getValuesCount() {
      return values_.size();
    }
    /**
     * <code>repeated int32 values = 1;</code>
     * @param index The index of the element to return.
     * @return The values at the given index.
     */
    public int getValues(int index) {
      return values_.getInt(index);
    }
    private int valuesMemoizedSerializedSize = -1;

    private byte memoizedIsInitialized = -1;
    @Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      getSerializedSize();
      if (getValuesList().size() > 0) {
        output.writeUInt32NoTag(10);
        output.writeUInt32NoTag(valuesMemoizedSerializedSize);
      }
      for (int i = 0; i < values_.size(); i++) {
        output.writeInt32NoTag(values_.getInt(i));
      }
      unknownFields.writeTo(output);
    }

    @Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      {
        int dataSize = 0;
        for (int i = 0; i < values_.size(); i++) {
          dataSize += com.google.protobuf.CodedOutputStream
            .computeInt32SizeNoTag(values_.getInt(i));
        }
        size += dataSize;
        if (!getValuesList().isEmpty()) {
          size += 1;
          size += com.google.protobuf.CodedOutputStream
              .computeInt32SizeNoTag(dataSize);
        }
        valuesMemoizedSerializedSize = dataSize;
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @Override
    public boolean equals(final Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof SortRequestResponse)) {
        return super.equals(obj);
      }
      SortRequestResponse other = (SortRequestResponse) obj;

      if (!getValuesList()
          .equals(other.getValuesList())) return false;
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      if (getValuesCount() > 0) {
        hash = (37 * hash) + VALUES_FIELD_NUMBER;
        hash = (53 * hash) + getValuesList().hashCode();
      }
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static SortRequestResponse parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static SortRequestResponse parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static SortRequestResponse parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static SortRequestResponse parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static SortRequestResponse parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static SortRequestResponse parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static SortRequestResponse parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static SortRequestResponse parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static SortRequestResponse parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static SortRequestResponse parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static SortRequestResponse parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static SortRequestResponse parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(SortRequestResponse prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code ru.ifmo.java.performance.protocol.SortRequestResponse}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:ru.ifmo.java.performance.protocol.SortRequestResponse)
        SortRequestResponseOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return Protocol.internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor;
      }

      @Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return Protocol.internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                SortRequestResponse.class, Builder.class);
      }

      // Construct using ru.ifmo.java.performance.protocol.Protocol.SortRequestResponse.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @Override
      public Builder clear() {
        super.clear();
        values_ = emptyIntList();
        bitField0_ = (bitField0_ & ~0x00000001);
        return this;
      }

      @Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return Protocol.internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor;
      }

      @Override
      public SortRequestResponse getDefaultInstanceForType() {
        return SortRequestResponse.getDefaultInstance();
      }

      @Override
      public SortRequestResponse build() {
        SortRequestResponse result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @Override
      public SortRequestResponse buildPartial() {
        SortRequestResponse result = new SortRequestResponse(this);
        int from_bitField0_ = bitField0_;
        if (((bitField0_ & 0x00000001) != 0)) {
          values_.makeImmutable();
          bitField0_ = (bitField0_ & ~0x00000001);
        }
        result.values_ = values_;
        onBuilt();
        return result;
      }

      @Override
      public Builder clone() {
        return super.clone();
      }
      @Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return super.setField(field, value);
      }
      @Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          Object value) {
        return super.addRepeatedField(field, value);
      }
      @Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof SortRequestResponse) {
          return mergeFrom((SortRequestResponse)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(SortRequestResponse other) {
        if (other == SortRequestResponse.getDefaultInstance()) return this;
        if (!other.values_.isEmpty()) {
          if (values_.isEmpty()) {
            values_ = other.values_;
            bitField0_ = (bitField0_ & ~0x00000001);
          } else {
            ensureValuesIsMutable();
            values_.addAll(other.values_);
          }
          onChanged();
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @Override
      public final boolean isInitialized() {
        return true;
      }

      @Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        SortRequestResponse parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (SortRequestResponse) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }
      private int bitField0_;

      private com.google.protobuf.Internal.IntList values_ = emptyIntList();
      private void ensureValuesIsMutable() {
        if (!((bitField0_ & 0x00000001) != 0)) {
          values_ = mutableCopy(values_);
          bitField0_ |= 0x00000001;
         }
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @return A list containing the values.
       */
      public java.util.List<Integer>
          getValuesList() {
        return ((bitField0_ & 0x00000001) != 0) ?
                 java.util.Collections.unmodifiableList(values_) : values_;
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @return The count of values.
       */
      public int getValuesCount() {
        return values_.size();
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @param index The index of the element to return.
       * @return The values at the given index.
       */
      public int getValues(int index) {
        return values_.getInt(index);
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @param index The index to set the value at.
       * @param value The values to set.
       * @return This builder for chaining.
       */
      public Builder setValues(
          int index, int value) {
        ensureValuesIsMutable();
        values_.setInt(index, value);
        onChanged();
        return this;
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @param value The values to add.
       * @return This builder for chaining.
       */
      public Builder addValues(int value) {
        ensureValuesIsMutable();
        values_.addInt(value);
        onChanged();
        return this;
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @param values The values to add.
       * @return This builder for chaining.
       */
      public Builder addAllValues(
          Iterable<? extends Integer> values) {
        ensureValuesIsMutable();
        com.google.protobuf.AbstractMessageLite.Builder.addAll(
            values, values_);
        onChanged();
        return this;
      }
      /**
       * <code>repeated int32 values = 1;</code>
       * @return This builder for chaining.
       */
      public Builder clearValues() {
        values_ = emptyIntList();
        bitField0_ = (bitField0_ & ~0x00000001);
        onChanged();
        return this;
      }
      @Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:ru.ifmo.java.performance.protocol.SortRequestResponse)
    }

    // @@protoc_insertion_point(class_scope:ru.ifmo.java.performance.protocol.SortRequestResponse)
    private static final SortRequestResponse DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new SortRequestResponse();
    }

    public static SortRequestResponse getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<SortRequestResponse>
        PARSER = new com.google.protobuf.AbstractParser<SortRequestResponse>() {
      @Override
      public SortRequestResponse parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new SortRequestResponse(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<SortRequestResponse> parser() {
      return PARSER;
    }

    @Override
    public com.google.protobuf.Parser<SortRequestResponse> getParserForType() {
      return PARSER;
    }

    @Override
    public SortRequestResponse getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    String[] descriptorData = {
      "\n\016protocol.proto\022!ru.ifmo.java.performan" +
      "ce.protocol\"%\n\023SortRequestResponse\022\016\n\006va" +
      "lues\030\001 \003(\005b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_ru_ifmo_java_performance_protocol_SortRequestResponse_descriptor,
        new String[] { "Values", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
