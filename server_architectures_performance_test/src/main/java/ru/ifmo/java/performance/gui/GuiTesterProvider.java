package ru.ifmo.java.performance.gui;

import ru.ifmo.java.performance.config.Config;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GuiTesterProvider {
    private Config config;

    private final Lock guiLock = new ReentrantLock();
    private final Condition guiEvent = guiLock.newCondition();

    private final Lock testerLock = new ReentrantLock();
    private final Condition testerEvent = testerLock.newCondition();

    private volatile boolean isGuiReady = false;
    private volatile boolean isTestReady = false;

    public GuiTesterProvider() {}

    public GuiTesterProvider(Config config) {
        this.config = config;
        isGuiReady = true;
        notifyGuiEvent();
    }

    public void notifyGuiEvent() {
        try {
            guiLock.lockInterruptibly();
            guiEvent.signalAll();
        } catch (InterruptedException e) {
            // ignore
        } finally {
            guiLock.unlock();
        }
    }

    public void notifyTesterEvent() {
        try {
            testerLock.lockInterruptibly();
            testerEvent.signalAll();
        } catch (InterruptedException e) {
            // ignore
        } finally {
            testerLock.unlock();
        }
    }

    public Lock getTesterLock() {
        return testerLock;
    }

    public Condition getTesterEvent() {
        return testerEvent;
    }

    public Lock getGuiLock() {
        return guiLock;
    }

    public Condition getGuiEvent() {
        return guiEvent;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public Config config() {
        return config;
    }

    public boolean isGuiReady() {
        return isGuiReady;
    }

    public void setGuiReady(boolean guiReady) {
        isGuiReady = guiReady;
    }

    public void setTestReady(boolean testReady) {
        isTestReady = testReady;
    }

    public boolean isTestReady() {
        return isTestReady;
    }
}
