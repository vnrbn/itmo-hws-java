package ru.ifmo.java.performance.servers;

import ru.ifmo.java.performance.config.ServerType;
import ru.ifmo.java.performance.servers.async.AsyncServer;
import ru.ifmo.java.performance.servers.block.BlockingServer;
import ru.ifmo.java.performance.servers.nonblock.NonBlockingServer;

import java.util.concurrent.CountDownLatch;

public class ServerFactory {
    public static Server newServerWithStartLatch(ServerType serverType, CountDownLatch latch, int tasksPoolSize) {
        switch (serverType) {
            case BLOCKING: {
                return new BlockingServer(latch, tasksPoolSize);
            }
            case NON_BLOCKING: {
                return new NonBlockingServer(latch, tasksPoolSize);
            }
            case ASYNC: {
                return new AsyncServer(latch, tasksPoolSize);
            }
            default: {
                throw new UnsupportedOperationException("unsupported server type: " + serverType.toString());
            }
        }
    }
}
