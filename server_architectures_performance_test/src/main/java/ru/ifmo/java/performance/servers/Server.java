package ru.ifmo.java.performance.servers;

import ru.ifmo.java.performance.utils.PerformanceTimer;

import java.util.Queue;

public interface Server extends Runnable {
    void shutDown();

    Queue<PerformanceTimer> getSortTimers();

    Queue<PerformanceTimer> getReceiveSendTimers();
}
