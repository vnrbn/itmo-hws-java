## Course work. Server architectures performance tester.

#### Server architectures
blocking

  - server - `ServerSocket`
  - connected clients - `Socket`

non-blocking 

  - server - `ServerSocketChannel` with `configureBlocking(false)`
  - connected clients - `SocketChannel` with `configureBlocking(false)`
  - two selectors: input and output
  
asynchronous

  - server - `AsynchronousServerSocketChannel`
  - connected clients - `AsynchronousSocketChannel`
  - read and write with `CompletionHandlers`
  
## Run
  `./run_performance_tester.sh`
  
  or 
  
  `java -jar performance-app.jar`


## Graphs data
[sheet_graphs.ods](GRAPHS/sheet_graphs.ods)

## Graphs
![](GRAPHS/client_request_process_time__array_size.png)
![](GRAPHS/client_request_process_time__clients_number.png)
![](GRAPHS/client_request_process_time__time_between_requests.png)
![](GRAPHS/server_request_process_time__array_size.png)
![](GRAPHS/server_request_process_time__clients_number.png)
![](GRAPHS/server_request_process_time__time_between_requests.png)
![](GRAPHS/server_sort_time__array_size.png)
![](GRAPHS/server_sort_time__clients_number.png)
![](GRAPHS/server_sort_time__time_between_requests.png)
