package ru.ifmo.java.torrent;

import ru.ifmo.java.torrent.server.Server;

import java.io.*;

public class ServerWorker {
    private final PipedOutputStream out = new PipedOutputStream();
    private final PipedInputStream in = new PipedInputStream(out);
    private final OutputStreamWriter writer = new OutputStreamWriter(out);

    private Thread thread;
    private Server server;

    public ServerWorker() throws IOException {}

    public void init() {
        server = new Server(in, out);
        thread = new Thread(server);
    }

    public void start() {
        thread.start();
    }

    public void send(String command) throws IOException {
        writer.write(command + "\n");
        writer.flush();
    }

    public void interrupt() {
        thread.interrupt();
    }

    public Server getServer() {
        return server;
    }
}
