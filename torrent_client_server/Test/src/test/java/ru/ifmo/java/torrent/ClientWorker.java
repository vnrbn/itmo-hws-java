package ru.ifmo.java.torrent;

import ru.ifmo.java.torrent.client.Client;

import java.io.*;

public class ClientWorker {
    private final PipedOutputStream out = new PipedOutputStream();
    private final PipedInputStream in = new PipedInputStream(out);
    private final OutputStreamWriter writer = new OutputStreamWriter(out);

    private final String loadFolder;

    private Thread thread;
    private Client client;

    public ClientWorker(String loadFolder) throws IOException {
        this.loadFolder = loadFolder;
    }

    public static Client createClient(InputStream in, OutputStream out, String loadFolder) {
        return new Client(in, out, loadFolder, "localhost", Constants.SERVER_TORRENT_PORT);
    }

    public void init() {
        client = createClient(in, out, loadFolder);
        thread = new Thread(client);
    }

    public void interrupt() {
        thread.interrupt();
    }

    public void start() {
        thread.start();
    }

    public void send(String command) throws IOException {
        writer.write(command + "\n");
        writer.flush();
    }

    public Client getClient() {
        return client;
    }
}