package ru.ifmo.java.torrent;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.Assert;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TrackerTests {
    private final String EXIT = "exit";
    private final String LOAD = "load";
    private final String UPLOAD = "upload";
    private final String TORRENT_DIR = "torrent_test";
    private final String SHARED_FILES_DIR = TORRENT_DIR + File.separator + "shared";
    private final String FILE = "file";

    public List<File> initFiles(int count, long lengthMB) throws IOException {
        LinkedList<File> files = new LinkedList<>();
        new File(SHARED_FILES_DIR).mkdirs();
        for (int i = 0; i < count; ++i) {
            String path = SHARED_FILES_DIR + File.separator + FILE + (i + 1);
            RandomAccessFile file = new RandomAccessFile(path, "rw");
            file.setLength(lengthMB * 1000 * 1000);
            file.close();
            files.add(new File(path));
        }
        return files;
    }

    public boolean isFilesEquals(File file1, File file2) throws IOException {
        if (file1.length() != file2.length()) {
            return false;
        }
        RandomAccessFile rfile1 = new RandomAccessFile(file1, "r");
        RandomAccessFile rfile2 = new RandomAccessFile(file2, "r");
        byte[] bytes1 = new byte[5 * 1000 * 1000];
        byte[] bytes2 = new byte[5 * 1000 * 1000];
        boolean isEqual = true;
        for (int i = 0; i < rfile1.length(); i+= bytes1.length) {
            rfile1.seek(i);
            rfile2.seek(i);
            rfile1.read(bytes1);
            rfile2.read(bytes2);
            isEqual = Arrays.equals(bytes1, bytes2);
            if (!isEqual) {
                return false;
            }
            bytes1 = new byte[5 * 1000 * 1000];
            bytes2 = new byte[5 * 1000 * 1000];
        }
        rfile1.close();
        rfile2.close();
        return isEqual;
    }

    public void makeFileNotEmpty(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
        String someString = "HELLO JAVA";
        for (int i = 0; i < 10; ++i) {
            someString += someString;
        }
        long length = randomAccessFile.length();
        for (long i = 0; i < (length + someString.length()) / someString.length(); ++i) {
            randomAccessFile.seek(i * someString.length());
            randomAccessFile.writeBytes(someString);
        }
        randomAccessFile.close();
    }

    public void clearDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(TORRENT_DIR));
        if (new File("server_dump").exists()) {
            new File("server_dump").delete();
        }
    }

    public static void clientWaiting(Integer time) throws InterruptedException{
        System.out.println("--- client waiting");
        Thread.sleep(time * 1000);
    }

    public static void clientToServerWaiting(Integer time) throws InterruptedException {
        System.out.println("--- client request to server waiting");
        Thread.sleep(time * 1000);
    }

    public static void serverWaiting(Integer time) throws InterruptedException {
        System.out.println("--- server waiting");
        Thread.sleep(time * 1000);
    }

    @Test
    public void uploadTest() throws IOException, InterruptedException {
        System.out.println("UPLOAD TEST START");
        clearDirectory();
        List<File> files = initFiles(2, 1);

        ClientWorker clientWorker1 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 1);
        ClientWorker clientWorker2 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 2);
        ServerWorker serverWorker = new ServerWorker();
        serverWorker.init();
        clientWorker1.init();
        clientWorker2.init();

        serverWorker.start();
        serverWaiting(5);

        clientWorker1.start();
        clientWorker2.start();
        clientWaiting(15);

        clientWorker1.send(UPLOAD + " " + files.get(0).getAbsolutePath());
        clientWorker2.send(UPLOAD + " " + files.get(1).getAbsolutePath());
        clientToServerWaiting(15);

        Assert.assertEquals(1, clientWorker1.getClient().getFiles().size());
        Assert.assertEquals(1, clientWorker2.getClient().getFiles().size());
        Assert.assertEquals(2, serverWorker.getServer().getFiles().size());

        clientWorker1.send("exit");
        clientWorker2.send("exit");
        serverWorker.send("exit");
        clientWaiting(10);

        clearDirectory();
        System.out.println("UPLOAD TEST END");
    }

    @Test
    public void loadTest() throws IOException, InterruptedException {
        System.out.println("LOAD TEST START");
        clearDirectory();
        List<File> files = initFiles(2, 100);
        for (File file : files) {
            makeFileNotEmpty(file);
        }

        ClientWorker clientWorker1 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 1);
        ClientWorker clientWorker2 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 2);
        ServerWorker serverWorker = new ServerWorker();
        serverWorker.init();
        clientWorker1.init();
        clientWorker2.init();

        serverWorker.start();
        serverWaiting(10);

        clientWorker1.start();
        clientWorker2.start();
        clientWaiting(15);

        clientWorker1.send(UPLOAD + " " + files.get(0).getAbsolutePath());
        clientToServerWaiting(10);

        clientWorker2.send(UPLOAD + " " + files.get(1).getAbsolutePath());
        clientToServerWaiting(10);

        clientWorker1.send(LOAD + " " + 2);
        clientToServerWaiting(20);
        clientWorker2.send(LOAD + " " + 1);
        clientToServerWaiting(20);

        File file1 = new File(TORRENT_DIR + File.separator + "client1" + File.separator + FILE + 2);
        File file2 = new File(TORRENT_DIR + File.separator + "client2" + File.separator + FILE + 1);

        Assert.assertEquals(2, clientWorker1.getClient().getFiles().size());
        Assert.assertEquals(2, clientWorker2.getClient().getFiles().size());
        Assert.assertTrue(file1.exists());
        Assert.assertTrue(file2.exists());
        Assert.assertTrue(isFilesEquals(file1, file2));

        clientWorker1.send("exit");
        clientWorker2.send("exit");
        serverWorker.send("exit");
        clientWaiting(10);

        clearDirectory();
        System.out.println("LOAD TEST START");
    }

    @Test
    public void reloadTest() throws IOException, InterruptedException {
        System.out.println("RELOAD TEST START");
        clearDirectory();
        List<File> files = initFiles(1, 1);
        for (File file : files) {
            makeFileNotEmpty(file);
        }

        ClientWorker clientWorker1 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 1);
        ServerWorker serverWorker1 = new ServerWorker();
        serverWorker1.init();
        clientWorker1.init();

        serverWorker1.start();
        serverWaiting(10);

        clientWorker1.start();
        clientWaiting(10);

        clientWorker1.send(UPLOAD + " " + files.get(0).getAbsolutePath());
        clientToServerWaiting(10);

        clientWorker1.send("exit");
        clientWaiting(5);
        serverWorker1.send("exit");
        serverWaiting(10);

        ClientWorker clientWorker2 = new ClientWorker(TORRENT_DIR + File.separator + "client" + 1);
        ServerWorker serverWorker2 = new ServerWorker();
        serverWorker2.init();
        serverWorker2.start();
        serverWaiting(5);

        clientWorker2.init();
        clientWorker2.start();
        clientWaiting(10);

        Assert.assertEquals(1, serverWorker2.getServer().getFiles().size());
        Assert.assertEquals(1, clientWorker2.getClient().getFiles().size());

        serverWorker2.send("exit");
        clientWorker2.send("exit");
        clientWaiting(10);

        clearDirectory();
        System.out.println("RELOAD TEST START");
    }

}
