package ru.ifmo.java.torrent.server;

import org.springframework.stereotype.Controller;
import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.client.ClientInfo;
import ru.ifmo.java.torrent.file.TorrentFile;
import ru.ifmo.java.torrent.utils.ConsolePrinter;
import ru.ifmo.java.torrent.utils.Source;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

@Controller
public class Server implements Runnable, ServerSerializer, ConsolePrinter {
    private final String HELP = "help";
    private final String FILES = "files";
    private final String EXIT = "exit";
    private final String DUMP_PATH = "server_dump";

    private final AtomicInteger id = new AtomicInteger(0);
    private final ExecutorService tasksPool = Executors.newCachedThreadPool();
    private final ExecutorService guysPool = Executors.newFixedThreadPool(2);
    private final ConcurrentLinkedQueue<String> log = new ConcurrentLinkedQueue<>();
    private final ConcurrentHashMap<Integer, TorrentFile> files = new ConcurrentHashMap<>();

    private final InputStream inputStream;
    private final OutputStream outputStream;

    private final ServerUpdater updater;
    private final ServerConnector connector = new ServerConnector(id, tasksPool, files, log);

    private boolean isInterrupted = false;

    public Server() {
        this.inputStream = System.in;
        this.outputStream = System.out;
        updater = new ServerUpdater(outputStream, files, log);
    }

    public Server(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        updater = new ServerUpdater(outputStream, files, log);
    }


    public static void main(String[] args) {
        new Server().run();
    }

    public ConcurrentHashMap<Integer, TorrentFile> getFiles() {
        return files;
    }

    @Override
    public void run() {
        deserialize();
        log.add("> server started");
        log.add("> print `help` to help");
        Scanner scanner = new Scanner(this.inputStream);
        guysPool.submit(connector);
        guysPool.submit(updater);
        try {
            while (!isInterrupted) {
                String command = scanner.nextLine();
                switch (command) {
                    case HELP:
                        processHelp();
                        break;
                    case FILES:
                        processFiles();
                        break;
                    case EXIT:
                        processExit();
                        break;
                    default:
                        log.add("> unknown command, use `help`");
                        break;
                }
            }
        } catch (Exception e) {
            log.add("SERVER EXCEPTION");
            e.printStackTrace();
        } finally {
            serialize();
            log.add("> server is done");
            showLog();
        }
    }

    public Source getAddress() {
        try {
            return new Source(InetAddress.getLocalHost().getAddress(), Constants.SERVER_TORRENT_PORT);
        } catch (UnknownHostException e) {
            log.add("> impossible get server address");
            return new Source(new byte[0], Constants.SERVER_TORRENT_PORT);
        }
    }

    private void showLog() {
        while(!log.isEmpty()) {
            cout(log.poll());
        }
    }

    private void processFiles() {
        StringBuilder logMessage = new StringBuilder();
        logMessage.append("Current files count ").append(files.size());
        for (Map.Entry<Integer, TorrentFile> fileEntry: files.entrySet()) {
            logMessage.append("\n").append(fileEntry.getValue().toString());
            for (ClientInfo clientInfo : fileEntry.getValue().getOwners().keySet()) {
                logMessage.append("\n\t").append(clientInfo.toString());
            }
        }
        log.add(logMessage.toString());
    }

    private void processExit() {
        isInterrupted = true;
        connector.interrupt();
        updater.interrupt();
        guysPool.shutdown();
        tasksPool.shutdown();
    }

    private void processHelp() {
        String logMessage = "Supported commands:\n" +
                "\tfiles -- show all files statistics\n" +
                "\texit -- close server";
        log.add(logMessage);
    }

    @Override
    public void serialize() {
        if (files.isEmpty()) {
            return;
        }
        File file = new File(DUMP_PATH);
        try {
            DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(file));
            file.createNewFile();
            outputStream.writeInt(files.size());
            for (Map.Entry<Integer, TorrentFile> fileEntry : files.entrySet()) {
                outputStream.writeInt(fileEntry.getKey());
                outputStream.writeLong(fileEntry.getValue().getSize());
                outputStream.writeUTF(fileEntry.getValue().getName());
            }
            outputStream.close();
            log.add("> info about " + files.size() + " files was dumped");
        } catch (IOException e) {
            log.add("SERIALIZATION EXCEPTION");
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize() {
        File file = new File(DUMP_PATH);
        if (!file.exists() || file.length() == 0) {
            log.add("> dump file doesn't exist or empty");
            return;
        }
        try {
            DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
            int maxId = 0;
            int filesCount = inputStream.readInt();
            for (int i = 0; i < filesCount; ++i) {
                int id = inputStream.readInt();
                long size = inputStream.readLong();
                String name = inputStream.readUTF();
                files.put(id, new TorrentFile(id, name, size));
                maxId = Math.max(id, maxId);
            }
            inputStream.close();
            id.set(maxId);
            log.add("> info about " + files.size() + " files was loaded");
        } catch (IOException e) {
            log.add("DESERIALIZATION EXCEPTION");
            e.printStackTrace();
        }
    }
}
