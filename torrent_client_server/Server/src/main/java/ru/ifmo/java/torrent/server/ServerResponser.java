package ru.ifmo.java.torrent.server;

import ru.ifmo.java.torrent.protocol.Torrent;

public interface ServerResponser {
    default Torrent.TrackerResponse buildResponse(Torrent.ListResponse response) {
        return Torrent.TrackerResponse.newBuilder()
                .setList(response)
                .build();
    }

    default Torrent.TrackerResponse buildResponse(Torrent.UpdateResponse response) {
        return Torrent.TrackerResponse.newBuilder()
                .setUpdate(response)
                .build();
    }

    default Torrent.TrackerResponse buildResponse(Torrent.SourcesResponse response) {
        return Torrent.TrackerResponse.newBuilder()
                .setSource(response)
                .build();
    }

    default Torrent.TrackerResponse buildResponse(Torrent.UploadResponse response) {
        return Torrent.TrackerResponse.newBuilder()
                .setUpload(response)
                .build();
    }
}
