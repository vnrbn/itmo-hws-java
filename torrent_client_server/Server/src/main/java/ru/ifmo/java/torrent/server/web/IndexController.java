package ru.ifmo.java.torrent.server.web;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.ifmo.java.torrent.file.TorrentFile;
import ru.ifmo.java.torrent.server.Server;
import ru.ifmo.java.torrent.utils.Source;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class IndexController {
    private final Server server = new Server();
    private final Thread serverThread = new Thread(server);

    public IndexController() {
        serverThread.start();
    }

    @GetMapping(value = "/")
    public String selectOptionExample1Page(Model model) {
        LoadForm form = new LoadForm();
        model.addAttribute("files", getServerFiles());
        model.addAttribute("loadForm", form);
        return "main-page";
    }

    @GetMapping(value = "/file.torrent", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getFile(@ModelAttribute("loadForm") LoadForm loadForm) {
        if (loadForm.getFileId() == null) {
            return new byte[0];
        }
        return getTorrentFile(loadForm.getFileId());
    }

    public List<LoadFile> getServerFiles() {
        return server.getFiles().values().stream()
                .filter(torrentFile -> torrentFile.getOwners().size() > 0)
                .map(tf -> new LoadFile(
                                tf.getId(),
                                tf.getName(),
                                tf.getOwners().size(),
                                tf.getSize()
                        )
                ).collect(Collectors.toList());
    }

    public byte[] getTorrentFile(Integer id)  {
        Source serverAddress = server.getAddress();
        TorrentFile file = server.getFiles().get(id);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(bos);
        try {
            dos.write(serverAddress.getIp());
            dos.writeInt(serverAddress.getPort());
            dos.writeInt(file.getId());
            dos.writeUTF(file.getName());
            dos.writeLong(file.getSize());
            dos.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
        return bos.toByteArray();
    }
}
