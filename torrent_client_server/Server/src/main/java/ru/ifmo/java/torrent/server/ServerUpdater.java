package ru.ifmo.java.torrent.server;

import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.client.ClientInfo;
import ru.ifmo.java.torrent.file.TorrentFile;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerUpdater implements Runnable, ConsolePrinter {
    private final long CHECK_TIME = 1 * 60 * 1000;

    private long lastCheck = System.currentTimeMillis();
    private final ConcurrentLinkedQueue<String> log;
    private final ConcurrentHashMap<Integer, TorrentFile> files;

    private final OutputStream outputStream;
    private final OutputStreamWriter writer;

    private boolean isInterrupted = false;

    public ServerUpdater(
            OutputStream outputStream,
            ConcurrentHashMap<Integer, TorrentFile> files,
            ConcurrentLinkedQueue<String> log
    ) {
        this.outputStream = outputStream;
        this.writer = new OutputStreamWriter(outputStream);
        this.files = files;
        this.log = log;
    }

    @Override
    public void run() {
        try {
            while (!isInterrupted) {
                showLog();
                checkClients();
            }
        } catch (Exception e) {
            log.add("UPDATER EXCEPTION");
            log.add(e.toString());
        } finally {
            showLog();
            try {
                writer.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }

    public void interrupt() {
        isInterrupted = true;
    }

    private void showLog() {
        try {
            while(!log.isEmpty()) {
                writer.write(log.poll() + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            // ignore
        }
    }

    private void checkClients() {
        if (System.currentTimeMillis() - lastCheck < CHECK_TIME) {
            return;
        }
        for (Map.Entry<Integer, TorrentFile> fileEntry : files.entrySet()) {
            List<ClientInfo> lostClients = new LinkedList<>();
            for (Map.Entry<ClientInfo, Long> clientEntry : fileEntry.getValue().getOwners().entrySet()) {
                if (System.currentTimeMillis() - clientEntry.getValue() > Constants.CLIENT_UPDATE_RANGE) {
                    lostClients.add(clientEntry.getKey());
                }
            }
            lostClients.forEach(client -> {
                fileEntry.getValue().getOwners().remove(client);
                log.add("> no updates from client " + client.toString() +
                        "in range " +  Constants.CLIENT_UPDATE_RANGE / 1000 / 60 + " minutes -- goodbye!");
            });
        }
    }
}
