package ru.ifmo.java.torrent.server;

import java.io.FileNotFoundException;

public interface ServerSerializer {
    void serialize();
    void deserialize();
}
