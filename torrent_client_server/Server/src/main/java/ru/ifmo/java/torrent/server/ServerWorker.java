package ru.ifmo.java.torrent.server;

import ru.ifmo.java.torrent.client.ClientInfo;
import ru.ifmo.java.torrent.file.TorrentFile;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerWorker implements Runnable, ServerResponser, ConsolePrinter {
    private final Socket socket;
    private final InputStream input;
    private final OutputStream output;

    private final AtomicInteger idCounter;
    private final ConcurrentLinkedQueue<String> log;
    private final ConcurrentHashMap<Integer, TorrentFile> files;
    private final ConcurrentHashMap<Integer, Integer> clientPortToSeederPort = new ConcurrentHashMap<>();

    private final String clientAddress;

    private byte[] clientIp;
    private int clientPort;

    private boolean isInterrupted = false;

    public ServerWorker(
            Socket socket,
            AtomicInteger idCounter,
            ConcurrentHashMap<Integer, TorrentFile> files,
            ConcurrentLinkedQueue<String> log
    ) throws IOException {
        this.socket = socket;
        this.idCounter = idCounter;
        this.files = files;
        this.log = log;
        this.input = socket.getInputStream();
        this.output = socket.getOutputStream();
        this.clientAddress = socket.getRemoteSocketAddress().toString();
    }

    @Override
    public void run() {
        try {
            clientIp = socket.getInetAddress().getAddress();
            clientPort = socket.getPort();
            while (!isInterrupted && !socket.isClosed()) {
                Torrent.TrackerRequest request = receiveRequest();
                switch (request.getRequestCase()) {
                    case LIST:
                        log.add("> list requested by " + clientAddress);
                        processList();
                        log.add("> list send to " + clientAddress);
                        break;
                    case UPLOAD:
                        log.add("> upload requested by" + clientAddress);
                        processUpload(request.getUpload());
                        log.add("> upload send to" + clientAddress);
                        break;
                    case SOURCE:
                        log.add("> sources requested " + clientAddress);
                        processSource(request.getSource());
                        log.add("> sources send to " + clientAddress);
                        break;
                    case UPDATE:
                        log.add("> update requested " + clientAddress +
                                " for " + request.getUpdate().getIdsList().toString() + " ids ");
                        processUpdate(request.getUpdate());
                        log.add("> update send to  " + clientAddress);
                        break;
                    default:
                        break;
                }
            }
        } catch (SocketException e) {
            log.add("> socket closed for client " + clientAddress);
        } catch (IOException e) {
            log.add("> work exception for client " + clientAddress);
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            processExit();
        }
    }

    public void interrupt() {
        isInterrupted = true;
        try {
            socket.close();
        } catch (IOException e) {
            log.add("> socket closed for client " + clientAddress);
        }
    }

    private Torrent.TrackerRequest receiveRequest() throws IOException {
        return Torrent.TrackerRequest.parseDelimitedFrom(input);
    }

    private void processList() throws IOException {
        Torrent.ListResponse.Builder response = Torrent.ListResponse.newBuilder();
        for (Map.Entry<Integer, TorrentFile> entry : files.entrySet()) {
            response.addFiles(entry.getValue().getListResponseFormat());
        }
        response.setCount(response.getFilesCount());
        buildResponse(response.build()).writeDelimitedTo(output);
    }

    private void processUpload(Torrent.UploadRequest request) throws IOException {
        int id = idCounter.incrementAndGet();
        TorrentFile file = new TorrentFile(id, request.getName(), request.getSize());
        files.put(id, file);
        buildResponse(file.getUploadResponseFormat()).writeDelimitedTo(output);
    }

    private void processSource(Torrent.SourcesRequest request) throws IOException {
        int id = request.getId();
        Torrent.SourcesResponse.Builder response = Torrent.SourcesResponse.newBuilder();
        if (files.containsKey(id)) {
            for (ClientInfo client : files.get(id).getOwners().keySet()) {
                response.addClients(client.getSourceResponseFormat());
            }
            response.setSize(response.getClientsCount());
        } else {
            response.setSize(0);
        }
        buildResponse(response.build()).writeDelimitedTo(output); // send
    }

    private void processUpdate(Torrent.UpdateRequest request) throws IOException {
        clientPortToSeederPort.put(clientPort, request.getClientPort());
        for (Integer id : request.getIdsList()) {
            TorrentFile file = files.get(id);
            if (file != null) {
                ClientInfo clientInfo = new ClientInfo(getRemoteIp(), request.getClientPort());
                file.getOwners().put(clientInfo, System.currentTimeMillis());
                log.add("> file " + file.toString() + " has " + file.getOwners().size() + " owners");
            }
        }
        buildResponse(Torrent.UpdateResponse.newBuilder().setStatus(true).build()).writeDelimitedTo(output);
    }

    private byte[] getRemoteIp() {
        return socket.getInetAddress().getAddress();
    }

    private void processExit() {
        log.add("> connection lost with " + clientAddress);
        Integer seederPort =  clientPortToSeederPort.get(clientPort);
        if (seederPort != null) {
            ClientInfo clientInfo = new ClientInfo(clientIp, seederPort);
            for (TorrentFile file : files.values()) {
                file.getOwners().remove(clientInfo);
            }
        }
    }
}

