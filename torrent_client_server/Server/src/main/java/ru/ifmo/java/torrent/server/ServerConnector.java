package ru.ifmo.java.torrent.server;

import ru.ifmo.java.torrent.file.TorrentFile;
import ru.ifmo.java.torrent.Constants;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class ServerConnector implements Runnable {
    private final AtomicInteger id;
    private final ExecutorService pool;
    private final ConcurrentLinkedQueue<String> log;
    private final ConcurrentHashMap<Integer, TorrentFile> files;

    private final List<ServerWorker> workerList = new LinkedList<>();

    private boolean isInterrupted = false;
    private ServerSocket serverSocket = null;

    public ServerConnector(
            AtomicInteger id,
            ExecutorService poll,
            ConcurrentHashMap<Integer, TorrentFile> files,
            ConcurrentLinkedQueue<String> log
    ) {
        this.id = id;
        this.pool = poll;
        this.files = files;
        this.log = log;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(Constants.SERVER_TORRENT_PORT)) {
            this.serverSocket = serverSocket;
            log.add("> server address is " +
                    serverSocket.getInetAddress().getHostAddress() + ":" + serverSocket.getLocalPort());
            while (!isInterrupted && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                ServerWorker worker = new ServerWorker(socket, id, files, log);
                pool.submit(worker);
                workerList.add(worker);
                log.add("> new client connected: " + socket.getRemoteSocketAddress());
            }
        } catch (SocketException e) {
            log.add("SERVER SOCKET WAS CLOSED");
        } catch (IOException e) {
            log.add("SERVER CONNECTOR EXCEPTION");
            e.printStackTrace();
        } finally {
            workerList.forEach(ServerWorker::interrupt);
        }
    }

    public void interrupt() {
        isInterrupted = true;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }
}
