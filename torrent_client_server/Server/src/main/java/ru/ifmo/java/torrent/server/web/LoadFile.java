package ru.ifmo.java.torrent.server.web;

public class LoadFile {
    private Integer id;
    private String name;
    private Integer owners;
    private Long size;

    public LoadFile(int id, String name, int owners, long size) {
        this.id = id;
        this.name = name;
        this.owners = owners;
        this.size = (1024 + size) / 1024;
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOwners() {
        return owners;
    }

    public Long getSize() {
        return size;
    }

    public void setOwners(Integer owners) {
        this.owners = owners;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return id + " - " + name + " [seeds: " + owners + ", size (KB): " + size + "]";
    }

}
