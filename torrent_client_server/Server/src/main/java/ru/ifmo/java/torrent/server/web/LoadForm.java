package ru.ifmo.java.torrent.server.web;

public class LoadForm {
    private Integer fileId;

    public LoadForm() {}

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }
}
