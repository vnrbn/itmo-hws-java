plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "6.1.0"
    id("com.google.protobuf") version "0.8.10" apply false
    id("org.springframework.boot") version "2.4.0"
    id("io.spring.dependency-management") version "1.0.10.RELEASE"
}

repositories {
    mavenCentral()
    maven {
        url = uri("https://maven.springframework.org/release")
    }
    maven {
        url = uri("http://repo.spring.io/milestone")
    }
    maven {
        url = uri("http://repo.spring.io/release")
    }
    maven {
        url = uri("http://repo.spring.io/snapshot")
    }
}

group = "ru.ifmo.java"
version = "1.0-SNAPSHOT"

subprojects {
    apply {
        plugin("java")
        plugin("application")
        plugin("com.github.johnrengelman.shadow")
    }

    repositories {
        mavenCentral()
        jcenter()
        maven {
            url = uri("https://maven.springframework.org/release")
        }
        maven {
            url = uri("http://repo.spring.io/milestone")
        }
        maven {
            url = uri("http://repo.spring.io/release")
        }
        maven {
            url = uri("http://repo.spring.io/snapshot")
        }
    }

    dependencies {
        implementation("com.google.protobuf:protobuf-java:3.10.0")
        implementation("org.jetbrains", "annotations", "17.0.0")
        testImplementation("junit", "junit", "4.12")
//
//        implementation("org.springframework.boot:spring-boot-starter-data-jpa")
//        implementation("org.springframework.boot:spring-boot-starter-jdbc")
//        implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
//        implementation("org.springframework.boot:spring-boot-starter-web")
//        runtimeOnly("org.postgresql:postgresql")
//        implementation("org.springframework.boot:spring-boot-starter-test")
//
//        implementation("javax.xml.bind:jaxb-api:2.3.0")
//        implementation("org.javassist:javassist:3.27.0-GA")
//        implementation("commons-io:commons-io:2.8.0")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}
