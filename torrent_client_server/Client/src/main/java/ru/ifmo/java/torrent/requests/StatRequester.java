package ru.ifmo.java.torrent.requests;

import ru.ifmo.java.torrent.utils.Source;
import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class StatRequester implements Runnable, ClientRequester {
    private final Source source;
    private final ClientRequest request;

    public StatRequester(Source source, Integer fileId) {
        this.source = source;
        this.request = new ClientRequest(getStatClientRequest(getStatRequest(fileId)));
    }

    @Override
    public void run() {
        try (Socket socket = new Socket(InetAddress.getByAddress(source.getIp()), source.getPort())) {
            request.process(socket.getInputStream(), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Torrent.ClientResponse getResponse() {
        return request.getResponse();
    }

    public Source getSource() {
        return source;
    }
}
