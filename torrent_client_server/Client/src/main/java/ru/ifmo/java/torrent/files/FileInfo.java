package ru.ifmo.java.torrent.files;

import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.utils.Source;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FileInfo {
    private final Integer id;
    private final String fileName;
    private final String filePath;
    private final Long size;

    private final int lastBlockSize;
    private final int blocksCount;

    private final CopyOnWriteArraySet<Source> sources = new CopyOnWriteArraySet<>();
    private final CopyOnWriteArraySet<Integer> loadedBlocks = new CopyOnWriteArraySet<>();
    private final ConcurrentLinkedQueue<Integer> blocksToLoad = new ConcurrentLinkedQueue<>();

    private boolean isLoaded;
    private final AtomicInteger leechWorkers = new AtomicInteger(0);
    private final AtomicInteger seedWorkers = new AtomicInteger(0);

    public FileInfo(
            Integer id,
            String filePath,
            String fileName,
            Long size,
            Boolean createNewFile
    ) throws IOException {
        this.id = id;
        this.filePath = filePath;
        if (fileName == null) {
            this.fileName = createFileNameFromPath(filePath);
        } else {
            this.fileName = fileName;
        }
        if (size == null) {
            this.isLoaded = true;
            this.size = new File(filePath).length();
        } else {
            this.isLoaded = false;
            this.size = size;
        }
        if (createNewFile) {
            RandomAccessFile f = new RandomAccessFile(filePath, "rw");
            f.setLength(this.size);
            f.close();
        }
        this.lastBlockSize = computeLastBlockSize(this.size);
        this.blocksCount = computeBlocksCount(this.size);
        initBlocks();
    }

    private void initBlocks() {
        if (isLoaded) {
            for (int i = 0; i < blocksCount; ++i) {
                loadedBlocks.add(i);
            }
        } else {
            for (int i = 0; i < blocksCount; ++i) {
                blocksToLoad.add(i);
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public String getFilePath() {
        return filePath;
    }

    public int registerLeechWorker() {
        return leechWorkers.incrementAndGet();
    }

    public int unregisterLeechWorker() {
        return leechWorkers.decrementAndGet();
    }

    public int registerSeedWorker() {
        return seedWorkers.incrementAndGet();
    }

    public int unregisterSeedWorker() {
        return seedWorkers.decrementAndGet();
    }

    public int getLeechWorkersCount() {
        return leechWorkers.get();
    }

    public CopyOnWriteArraySet<Integer> getLoadedBlocks() {
        return loadedBlocks;
    }

    public CopyOnWriteArraySet<Source> getSources() {
        return sources;
    }

    public boolean isLoaded() {
        return loadedBlocks.size() == blocksCount;
    }

    public void serialize(DataOutputStream outputStream) throws IOException {
        outputStream.writeInt(id);
        outputStream.writeLong(size);
        outputStream.writeInt(blocksCount);
        outputStream.writeUTF(fileName);
        outputStream.writeUTF(filePath);
        outputStream.writeBoolean(isLoaded);
        if (!isLoaded) {
            outputStream.writeInt(loadedBlocks.size());
            for (Integer id : loadedBlocks) {
                outputStream.writeInt(id);
            }
        }
    }

    public static FileInfo deserialize(DataInputStream inputStream) throws IOException {
        Integer id = inputStream.readInt();
        Long size = inputStream.readLong();
        int blocksCount = inputStream.readInt();
        String fileName = inputStream.readUTF();
        String filePath = inputStream.readUTF();
        boolean isLoaded = inputStream.readBoolean();
        Integer loadedBlocksCount = null;
        List<Integer> loadedBlocks = new LinkedList<>();
        if (!isLoaded) {
            loadedBlocksCount = inputStream.readInt();
            for (int i = 0; i < loadedBlocksCount; ++i) {
                loadedBlocks.add(inputStream.readInt());
            }
        }
        FileInfo fileInfo = new FileInfo(id, filePath, fileName, size, false);
        List<Integer> blocks = new LinkedList<>();
        for (int i = 0; i < blocksCount; ++i) {
            blocks.add(i);
        }
        fileInfo.isLoaded = isLoaded;
        fileInfo.loadedBlocks.clear();
        fileInfo.blocksToLoad.clear();
        if (isLoaded) {
            fileInfo.loadedBlocks.addAll(blocks);
        } else {
            fileInfo.loadedBlocks.addAll(loadedBlocks);
            fileInfo.blocksToLoad.addAll(blocks.stream().filter(blockId ->
                    !fileInfo.loadedBlocks.contains(blockId)
            ).collect(Collectors.toSet()));
        }
        return fileInfo;
    }

    public byte[] getBlock(Integer id) throws IOException {
        if (!loadedBlocks.contains(id)) {
            return new byte[0];
        }
        long pos = id * (long) Constants.BLOCK_SIZE;
        int length = getBlockLength(id);
        byte[] buffer = new byte[length];
        RandomAccessFile file = new RandomAccessFile(filePath, "r");
        file.seek(pos);
        file.read(buffer);
        file.close();
        return buffer;
    }

    public boolean writeBlock(Integer id, byte[] block) throws IOException {
        if (loadedBlocks.contains(id)) {
            return false;
        }
        long pos = id * Constants.BLOCK_SIZE;
        RandomAccessFile file = new RandomAccessFile(filePath, "rw");
        file.seek(pos);
        file.write(block);
        file.close();
        blocksToLoad.remove(id);
        loadedBlocks.add(id);
        checkStatus();
        return true;
    }

    private void checkStatus() {
        if (loadedBlocks.size() == blocksCount) {
            isLoaded = true;
            System.out.println("> file wad loaded " + toString());
        }
    }

    private int getBlockLength(Integer id) {
        if (id + 1 == blocksCount) {
            return lastBlockSize;
        }
        return (int) Constants.BLOCK_SIZE;
    }

    @Override
    public String toString() {
        return "[id: " + id + ", name: " + fileName +
                ", path: " + filePath + ", size: " + size +
                ", blocks: " + loadedBlocks.size() + "/" + blocksCount + "]";
    }

    public static int computeBlocksCount(long fileSize) {
        int blocksCount = 0;
        if (fileSize % Constants.BLOCK_SIZE == 0) {
            blocksCount = (int) (fileSize / Constants.BLOCK_SIZE);
        } else {
            blocksCount = (int) ((fileSize + Constants.BLOCK_SIZE) / Constants.BLOCK_SIZE);
        }
        return blocksCount;
    }

    public static int computeLastBlockSize(long fileSize) {
        if (fileSize % Constants.BLOCK_SIZE == 0) {
            return (int) Constants.BLOCK_SIZE;
        }
        return (int) (fileSize % Constants.BLOCK_SIZE);
    }

    public String getFileName() {
        return fileName;
    }

    public Long getSize() {
        return size;
    }

    public int getBlocksCount() {
        return blocksCount;
    }

    public int getSeedWorkersCount() {
        return seedWorkers.get();
    }

    public static String createFileNameFromPath(String filePath) {
        int idx = filePath.lastIndexOf(File.separator);
        String name = null;
        if (idx < 0) {
            name = filePath;
        } else if (!filePath.substring(idx + 1).isEmpty()) {
            name = filePath.substring(idx + 1);
        }
        return name;
    }
}
