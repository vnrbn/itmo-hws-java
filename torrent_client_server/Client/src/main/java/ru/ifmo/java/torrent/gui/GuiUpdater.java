package ru.ifmo.java.torrent.gui;

import ru.ifmo.java.torrent.client.Client;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.Queue;

public class GuiUpdater implements Runnable {
    private final long UPDATES_RANGE = 1000;

    private final GuiClient gui;
    private final Client client;
    private final Queue<String> backLog;
    private final Queue<String> guiLog;
    private JTextArea logArea;

    private volatile boolean isInterrupted = false;

    public GuiUpdater(GuiClient gui, Client client) {
        this.gui = gui;
        this.client = client;
        this.backLog = client.getSecondLog();
        this.guiLog = gui.getGuiLog();
    }

    @Override
    public void run() {
        try {
            logArea = gui.getLogArea();
            int serverPort = client.getServerPort();
            while (!isInterrupted) {
                gui.getTable().revalidate();
                ((AbstractTableModel) gui.getTable().getModel()).fireTableDataChanged();
                if (client.isRunning()) {
                    gui.getStatusLabel().setText("connected to: " + client.getServerIp() + ":" + serverPort);
                    gui.getStatusLabel().setForeground(Color.GREEN);
                } else {
                    gui.getStatusLabel().setText("disconnected");
                    gui.getStatusLabel().setForeground(Color.RED);
                }
                updateLog();
                Thread.sleep(UPDATES_RANGE);
            }
        } catch (InterruptedException e) {
            // ignore
        } finally {
            isInterrupted = true;
        }
    }

    public void updateLog() {
        while(!guiLog.isEmpty()) {
            logArea.setText(guiLog.poll() + "\n" + logArea.getText());
        }
        while (!backLog.isEmpty()) {
            logArea.setText(backLog.poll() + "\n" + logArea.getText());
        }
    }

    public void interrupt() {
        isInterrupted = true;
    }
}
