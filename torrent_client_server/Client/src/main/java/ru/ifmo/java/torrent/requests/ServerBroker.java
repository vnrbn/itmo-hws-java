package ru.ifmo.java.torrent.requests;

import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerBroker implements Runnable, ConsolePrinter {
    private Socket socket = null;
    private InputStream input = null;
    private OutputStream output = null;

    private final String serverIp;
    private final int serverPort;
    private final Queue<String> log;
    private final ConcurrentLinkedQueue<ServerRequest> requests = new ConcurrentLinkedQueue<>();

    private boolean isInterrupted = false;

    public ServerBroker(String ip, int serverPort, Queue<String> logQueue) {
        this.serverIp = ip;
        this.serverPort = serverPort;
        this.log = logQueue;
    }

    @Override
    public void run() {
        try {
            initSocket();
            log.add("> broker is ready");
            while (!isInterrupted && !socket.isClosed()) {
                ServerRequest request = requests.poll();
                if (request != null) {
                    request.process(input, output);
                }
            }
        } catch (SocketException e) {
            log.add("BROKER SOCKET IS CLOSED");
        } catch (IOException e) {
            log.add("BROKER EXCEPTION");
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                log.add("> server broker is dead :/");
            }
            isInterrupted = true;
        }
    }

    public boolean isInterrupted() {
        return isInterrupted;
    }


    public void addRequest(ServerRequest request) {
        requests.add(request);
    }

    private void initSocket() throws IOException {
        socket = new Socket(serverIp, serverPort);
        input = socket.getInputStream();
        output = socket.getOutputStream();
    }

    public void interrupt() {
        isInterrupted = true;
        try {
            socket.close();
        } catch (IOException e) {
            // ignore
        }
    }

    public String getServerIp() {
        return serverIp;
    }

    public int getServerPort() {
        return serverPort;
    }
}
