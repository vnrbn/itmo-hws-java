package ru.ifmo.java.torrent.requests;

import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ClientRequest implements Request, ClientRequester {
    private final Lock requestLock = new ReentrantLock();
    private final Condition ready = requestLock.newCondition();

    private final Torrent.ClientRequest request;
    private Torrent.ClientResponse response = null;

    private boolean isReady = false;

    public ClientRequest(Torrent.ClientRequest request) {
        this.request = request;
    }

    @Override
    public void process(InputStream input, OutputStream output) throws IOException {
        if (!isReady) {
            try {
                requestLock.lockInterruptibly();
                response = requestResponse(input, output, request);
            } catch (InterruptedException e) {
                // ignore
            } finally {
                isReady = true;
                ready.signalAll();
                requestLock.unlock();
            }
        }
    }
    public Torrent.ClientResponse getResponse() {
        if (!isReady) {
            requestLock.lock();
            try {
                while (!isReady) {
                    ready.await();
                }
            } catch (InterruptedException e) {
                // ignore
            } finally {
                isReady = true;
                requestLock.unlock();
            }
        }
        return response;
    }

    public boolean isReady() {
        return isReady;
    }
}
