package ru.ifmo.java.torrent.gui;

import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.client.Client;
import ru.ifmo.java.torrent.files.DotTorrentFile;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GuiClient implements Runnable {
    private final int LOAD_FRAME_WIDTH = 600;
    private final int LOAD_FRAME_HEIGHT = 200;
    private final int MAIN_FRAME_WIDTH = 800;
    private final int MAIN_FRAME_HEIGHT = 800;
    private final String GUI_NAME = "Torrent Client Java - Ivan Rybin MA SE ITMO & JB";

    private final JFrame dirSelectFrame = new JFrame();
    private final JFrame loadFrame = new JFrame();
    private final JFrame mainFrame = new JFrame();
    private final JPanel mainPanel = new JPanel();
    private final JLabel statusLabel = new JLabel();
    private final JTable filesTable = new JTable();
    private final JTextArea logArea = new JTextArea();
    private final JScrollPane logPane = new JScrollPane(logArea);

    private final ExecutorService pool = Executors.newFixedThreadPool(3);
    private final ConcurrentLinkedQueue<String> guiLog = new ConcurrentLinkedQueue<>();

    private DotTorrentFile dotTorrentFile = null;
    private String loadFolder = null;
    private Client client = null;

    private final Lock lock = new ReentrantLock();
    private final Condition guiReady = lock.newCondition();
    private volatile boolean mainFrameLoaded = false;

    private GuiUpdater updater = null;

    public GuiClient() {}

    public static void main(String[] args) {
        new GuiClient().run();
    }

    @Override
    public void run() {
        loadDirSelectPage();
        try {
            lock.lockInterruptibly();
            while (!mainFrameLoaded) {
                guiReady.await();
            }
            if (dotTorrentFile != null) {
                client = new Client(System.in, System.out, loadFolder,
                        dotTorrentFile.getStringIp(), dotTorrentFile.getPort());
            } else {
                client = new Client(System.in, System.out, loadFolder,
                        "localhost", Constants.SERVER_TORRENT_PORT);
            }
            pool.submit(client);
            pool.submit(() -> {
                while (!client.isRunning()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }
                if (dotTorrentFile != null) {
                    client.loadFile(dotTorrentFile.getId());
                }
            });
            this.filesTable.setModel(new FilesTableModel(client.getFiles()));
            this.updater = new GuiUpdater(this, client);
            pool.submit(this.updater);
        } catch (InterruptedException | UnknownHostException e) {
            dirSelectFrame.dispose();
            loadFrame.dispose();
            mainFrame.dispose();
            updater.interrupt();
            pool.shutdown();
        } finally {
            lock.unlock();
        }
    }

    private void loadDirSelectPage() {
        dirSelectFrame.setTitle(GUI_NAME);
        dirSelectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dirSelectFrame.setSize(LOAD_FRAME_WIDTH, LOAD_FRAME_HEIGHT);
        initSelectButton();
        dirSelectFrame.setLocation(getFrameCenterPoint(loadFrame));
        dirSelectFrame.setVisible(true);
    }

    private void loadStartPage() {
        loadFrame.setTitle(GUI_NAME);
        loadFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loadFrame.setSize(LOAD_FRAME_WIDTH, LOAD_FRAME_HEIGHT);
        loadFrame.setLayout(new GridLayout());
        initDefaultConnectionButton();
        initDotTorrentConnectionButton();
        loadFrame.setLocation(getFrameCenterPoint(loadFrame));
        loadFrame.setVisible(true);
    }

    private void loadMainPage() {
        mainFrame.setTitle(GUI_NAME);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
        mainFrame.setLayout(new BorderLayout(20, 20));
        mainPanel.setLayout(new GridLayout());
        initUploadButton();
        initLoadButton();
        initTableGuiPart();
        initLabels();
        initLogArea();
        mainFrame.setLocation(getFrameCenterPoint(mainFrame));
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                pool.submit(() -> {
                    if (client != null) {
                        client.processExit();
                    }
                    pool.shutdown();
                    mainFrame.dispose();
                });
            }
        });
        mainFrame.setVisible(true);
        mainFrameLoaded = true;
        try {
            lock.lockInterruptibly();
            guiReady.signalAll();
        } catch (InterruptedException e){
            // ignore
        } finally {
            lock.unlock();
        }
    }

    private void initSelectButton() {
        JButton button = new JButton("select downloads folder");
        button.addActionListener(actionEvent -> {
            File folder = loadWithChooser(JFileChooser.DIRECTORIES_ONLY);
            if (folder != null) {
                loadFolder = folder.getAbsolutePath();
                dirSelectFrame.dispose();
                loadStartPage();
            }
        });
        dirSelectFrame.add(button);
    }

    private void initDefaultConnectionButton() {
        JButton button = new JButton("default server connection");
        button.addActionListener(actionEvent -> {
            loadFrame.dispose();
            loadMainPage();
            try {
                lock.lockInterruptibly();
                guiReady.signalAll();
            } catch (InterruptedException e) {
                // ignore
            } finally {
                lock.unlock();
            }
        });
        loadFrame.add(button);
    }

    private void initDotTorrentConnectionButton() {
        JButton button = new JButton(".torrent connection");
        button.addActionListener(actionEvent -> {
            File file = loadWithChooser(JFileChooser.FILES_ONLY);
            if (file != null) {
                guiLog.add("> chosen .torrent file " + file.getAbsolutePath());
                dotTorrentFile = DotTorrentFile.parse(file);
                pool.submit(() -> {
                    loadFrame.dispose();
                    loadMainPage();
                    try {
                        lock.lockInterruptibly();
                        guiReady.signalAll();
                    } catch (InterruptedException e) {
                        // ignore
                    } finally {
                        lock.unlock();
                    }
                });
            }
        });
        loadFrame.add(button);
    }

    private void initUploadButton() {
        JButton uploadButton = new JButton("upload file");
        uploadButton.setPreferredSize(new Dimension(100, 50));
        uploadButton.addActionListener(actionEvent -> {
            File file = loadWithChooser(JFileChooser.FILES_ONLY);
            if (file != null) {
                guiLog.add("> chosen upload file " + file.getAbsolutePath());
                pool.submit(() -> {
                    try {
                        client.uploadFile(file.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
        mainPanel.add(uploadButton);
        mainFrame.getContentPane().add(mainPanel, BorderLayout.PAGE_START);
    }

    private void initLoadButton() {
        JButton loadButton = new JButton("load via .torrent");
        loadButton.setPreferredSize(new Dimension(100, 50));
        loadButton.addActionListener(actionEvent -> {
            File file = loadWithChooser(JFileChooser.FILES_ONLY);
            if (file != null) {
                guiLog.add("> chosen .torrent file " + file.getAbsolutePath());
                pool.submit(() -> {
                    try {
                        DotTorrentFile dotTorrentFile = DotTorrentFile.parse(file);
                        if (client.checkIpPortFile(dotTorrentFile.getStringIp(), dotTorrentFile.getPort())) {
                            client.loadFile(dotTorrentFile.getId());
                        } else {
                            guiLog.add("> .torrent file is not from current tracker");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        });
        mainPanel.add(loadButton);
        mainFrame.getContentPane().add(mainPanel, BorderLayout.PAGE_START);
    }

    private void initLabels() {
        statusLabel.setVerticalAlignment(JLabel.CENTER);
        statusLabel.setHorizontalAlignment(JLabel.CENTER);
        statusLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        mainPanel.add(statusLabel);
    }

    private void initLogArea() {
        mainFrame.getContentPane().add(logPane, BorderLayout.CENTER);
    }

    private void initTableGuiPart() {
        JScrollPane jScrollPane = new JScrollPane(this.filesTable);
        jScrollPane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Torrent files", TitledBorder.LEFT,
                TitledBorder.TOP));
        mainFrame.getContentPane().add(jScrollPane, BorderLayout.PAGE_END);
    }

    private Point getFrameCenterPoint(JFrame frame) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Point mid = new Point(screenSize.width / 2, screenSize.height / 2);
        return new Point(mid.x - (frame.getWidth() / 2),mid.y - (frame.getHeight() / 2));
    }

    private File loadWithChooser(int FILES_TYPE) {
        JFileChooser f = new JFileChooser();
        f.setCurrentDirectory(new File(System.getProperty("user.dir")));
        f.setFileSelectionMode(FILES_TYPE);
        f.showSaveDialog(null);
        return f.getSelectedFile();
    }

    public JTable getTable() {
        return filesTable;
    }

    public JLabel getStatusLabel() {
        return statusLabel;
    }

    public JTextArea getLogArea() {
        return logArea;
    }

    public Queue<String> getGuiLog() {
        return guiLog;
    }
}
