package ru.ifmo.java.torrent.requests;

import com.google.protobuf.ByteString;
import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface ClientRequester {
    default Torrent.ClientResponse requestResponse(
            InputStream input,
            OutputStream output,
            Torrent.ClientRequest request
    ) throws IOException {
        sendRequest(output, request);
        return Torrent.ClientResponse.parseDelimitedFrom(input);
    }


    default Torrent.ClientRequest getStatClientRequest(Torrent.StatRequest request) {
        return Torrent.ClientRequest.newBuilder()
                .setStat(request)
                .build();
    }

    default Torrent.ClientRequest getGetClientRequest(Torrent.GetRequest request) {
        return Torrent.ClientRequest.newBuilder()
                .setGet(request)
                .build();
    }

    default Torrent.ClientResponse getGetClientResponse(Torrent.GetResponse response) {
        return Torrent.ClientResponse.newBuilder()
                .setGet(response)
                .build();
    }

    default Torrent.ClientResponse getStatClientResponse(Torrent.StatResponse response) {
        return Torrent.ClientResponse.newBuilder()
                .setStat(response)
                .build();
    }

    default Torrent.StatResponse statRequestResponse(
            InputStream input,
            OutputStream output,
            Integer fileId
    ) throws IOException {
        sendRequest(output, getStatRequest(fileId));
        return Torrent.StatResponse.parseDelimitedFrom(input);
    }

    default Torrent.GetResponse getRequestResponse(
            InputStream input,
            OutputStream output,
            Integer fileId,
            Integer blockId
    ) throws IOException {
        sendRequest(output, getGetRequest(fileId, blockId));
        return Torrent.GetResponse.parseDelimitedFrom(input);
    }

    default Torrent.StatRequest getStatRequest(Integer fileId) {
        return Torrent.StatRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.STAT_BYTES]))
                .setId(fileId)
                .build();
    }

    default Torrent.GetRequest getGetRequest(Integer fileId, Integer blockId) {
        return Torrent.GetRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.GET_BYTES]))
                .setId(fileId)
                .setPart(blockId)
                .build();
    }

    default void sendRequest(OutputStream output, Torrent.ClientRequest request) throws IOException {
        request.writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.StatRequest request) throws IOException {
        Torrent.ClientRequest.newBuilder()
                .setStat(request)
                .build()
                .writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.GetRequest request) throws IOException {
        Torrent.ClientRequest.newBuilder()
                .setGet(request)
                .build()
                .writeDelimitedTo(output);
    }
}
