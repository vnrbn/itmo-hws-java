package ru.ifmo.java.torrent.client;

public interface ClientSerializer {
    void serialize();
    void deserialize();
}
