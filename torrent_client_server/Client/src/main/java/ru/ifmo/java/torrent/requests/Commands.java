package ru.ifmo.java.torrent.requests;

public class Commands {
    public static final String HELP = "help";
    public static final String LIST = "list";
    public static final String LOAD = "load";
    public static final String UPLOAD = "upload";
    public static final String STATUS = "status";
    public static final String SOURCES = "sources";
    public static final String EXIT = "exit";
}
