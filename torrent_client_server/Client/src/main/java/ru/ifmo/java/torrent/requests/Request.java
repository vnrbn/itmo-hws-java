package ru.ifmo.java.torrent.requests;

import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Request {
    void process(InputStream input, OutputStream output) throws IOException;
}