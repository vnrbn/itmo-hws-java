package ru.ifmo.java.torrent.requests;

import com.google.protobuf.ByteString;
import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;

public interface ServerRequester {
    default Torrent.TrackerResponse requestResponse(
            InputStream input,
            OutputStream output,
            Torrent.TrackerRequest request
    ) throws IOException {
        sendRequest(output, request);
        return Torrent.TrackerResponse.parseDelimitedFrom(input);
    }

    default Torrent.ListRequest getListRequest() {
        return Torrent.ListRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.LIST_BYTES]))
                .build();
    }

    default Torrent.UpdateRequest getUpdateRequest(int port, Set<Integer> ids) {
        return Torrent.UpdateRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.UPDATE_BYTES]))
                .setClientPort(port)
                .setCount(ids.size())
                .addAllIds(ids)
                .build();
    }

    default Torrent.SourcesRequest getSourcesRequest(Integer id) {
        return Torrent.SourcesRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.SOURCES_BYTES]))
                .setId(id)
                .build();
    }

    default Torrent.UploadRequest getUploadRequest(String fileName, String filePath) {
        return Torrent.UploadRequest.newBuilder()
                .setBytes(ByteString.copyFrom(new byte[Constants.UPLOAD_BYTES]))
                .setName(fileName)
                .setSize(new File(filePath).length())
                .build();
    }


    default Torrent.TrackerRequest buildRequest(Torrent.ListRequest request) {
        return Torrent.TrackerRequest.newBuilder()
                .setList(request)
                .build();
    }

    default Torrent.TrackerRequest buildRequest(Torrent.UpdateRequest request) {
        return Torrent.TrackerRequest.newBuilder()
                .setUpdate(request)
                .build();
    }

    default Torrent.TrackerRequest buildRequest(Torrent.SourcesRequest request) {
        return Torrent.TrackerRequest.newBuilder()
                .setSource(request)
                .build();
    }

    default Torrent.TrackerRequest buildRequest(Torrent.UploadRequest request) {
        return Torrent.TrackerRequest.newBuilder()
                .setUpload(request)
                .build();
    }

    default void sendRequest(OutputStream output, Torrent.TrackerRequest request) throws IOException {
        request.writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.ListRequest request) throws IOException {
        Torrent.TrackerRequest.newBuilder()
                .setList(request)
                .build()
                .writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.UpdateRequest request) throws IOException {
        Torrent.TrackerRequest.newBuilder()
                .setUpdate(request)
                .build()
                .writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.SourcesRequest request) throws IOException {
        Torrent.TrackerRequest.newBuilder()
                .setSource(request)
                .build()
                .writeDelimitedTo(output);
    }

    default void sendRequest(OutputStream output, Torrent.UploadRequest request) throws IOException {
        Torrent.TrackerRequest.newBuilder()
                .setUpload(request)
                .build()
                .writeDelimitedTo(output);
    }

    default Torrent.ListResponse listRequestResponse(
            InputStream input,
            OutputStream output
    ) throws IOException {
        sendRequest(output, getListRequest());
        return Torrent.ListResponse.parseDelimitedFrom(input);
    }

    default Torrent.UploadResponse uploadRequestResponse(
            InputStream input,
            OutputStream output,
            String fileName,
            String filePath
    ) throws IOException {
        sendRequest(output, getUploadRequest(fileName, filePath));
        return Torrent.UploadResponse.parseDelimitedFrom(input);
    }

    default Torrent.SourcesResponse sourcesRequestResponse(
            InputStream input,
            OutputStream output,
            Integer id
    ) throws IOException {
        sendRequest(output, getSourcesRequest(id));
        return Torrent.SourcesResponse.parseDelimitedFrom(input);
    }

    default Torrent.UpdateResponse updateRequestResponse(
            InputStream input,
            OutputStream output,
            int port,
            Set<Integer> ids
    ) throws IOException {
        sendRequest(output, getUpdateRequest(port, ids));
        return Torrent.UpdateResponse.parseDelimitedFrom(input);
    }
}
