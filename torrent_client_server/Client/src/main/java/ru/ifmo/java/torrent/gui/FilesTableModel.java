package ru.ifmo.java.torrent.gui;

import ru.ifmo.java.torrent.files.FileInfo;

import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class FilesTableModel extends AbstractTableModel {
    private final int COLUMNS_COUNT = 7;

    private Set<TableModelListener> listeners = new HashSet<>();
    private Map<Integer, FileInfo> files;

    public FilesTableModel(Map<Integer, FileInfo> files) {
        this.files = files;
    }

    @Override
    public int getRowCount() {
        return files.size();
    }

    @Override
    public int getColumnCount() {
        return COLUMNS_COUNT;
    }

    @Override
    public String getColumnName(int i) {
        switch (i) {
            case 0: return "id";
            case 1: return "name";
            case 2: return "size";
            case 3: return "blocks loaded";
            case 4: return "# l";
            case 5: return "# s";
            case 6: return "path";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int i) {
        switch (i) {
            case 0: return int.class;
            case 1: return String.class;
            case 2: return Long.class;
            case 3: return String.class;
            case 4: return int.class;
            case 5: return int.class;
            case 6: return String.class;
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int fileIdx, int valueIdx) {
        return false;
    }

    @Override
    public Object getValueAt(int fileIdx, int valueIdx) {
        FileInfo file = new ArrayList<>(files.values()).get(fileIdx);
        switch (valueIdx) {
            case 0: return file.getId();
            case 1: return file.getFileName();
            case 2: return file.getSize();
            case 3: return file.getLoadedBlocks().size() + "/" + file.getBlocksCount();
            case 4: return file.getLeechWorkersCount();
            case 5: return file.getSeedWorkersCount();
            case 6: return file.getFilePath();
        }
        return "";
    }

    @Override
    public void setValueAt(Object o, int i, int i1) {}

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        listeners.add(tableModelListener);
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {
        listeners.remove(tableModelListener);
    }
}
