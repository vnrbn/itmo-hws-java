package ru.ifmo.java.torrent.client;

import ru.ifmo.java.torrent.Constants;
import ru.ifmo.java.torrent.files.FileInfo;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.requests.ServerBroker;
import ru.ifmo.java.torrent.requests.ServerRequest;
import ru.ifmo.java.torrent.requests.ServerRequester;
import ru.ifmo.java.torrent.utils.ConsolePrinter;
import ru.ifmo.java.torrent.utils.TorrentUtils;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static ru.ifmo.java.torrent.requests.Commands.*;

public class Client implements Runnable, ClientSerializer, ServerRequester, ConsolePrinter {
    private final String DUMP_FILE = "client_dump";
    private final Set<String> uploadedFiles = new HashSet<>();
    private final ConcurrentHashMap<Integer, FileInfo> files = new ConcurrentHashMap<>();

    private final ExecutorService guysPool = Executors.newFixedThreadPool(3);
    private final ExecutorService brokerPool = Executors.newSingleThreadExecutor();
    private final ExecutorService tasksPool = Executors.newFixedThreadPool(Constants.CLIENT_THREADS_CNT);

    private Seeder seeder;
    private Leecher leecher;
    private ClientUpdater clientUpdater;
    private ServerBroker broker;

    private InputStream inputStream;
    private OutputStream outputStream;

    private final ConcurrentLinkedQueue<String> mainLog = new ConcurrentLinkedQueue<>();
    private final ConcurrentLinkedQueue<String> secondLog = new ConcurrentLinkedQueue<>();

    private boolean alive = true;
    private String loadFolderPath;

    private volatile boolean isRunning = false;

    public Client(
            InputStream inputStream,
            OutputStream outputStream,
            String loadFolderPath,
            String serverIp,
            int serverPort
    ) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.broker = new ServerBroker(serverIp, serverPort, mainLog);
        this.loadFolderPath = loadFolderPath;
        new File(loadFolderPath).mkdirs();
        int seederPort;
        try {
            seederPort = TorrentUtils.findFreePort();
            mainLog.add("> seeder port is " + seederPort);
        } catch (IOException e) {
            mainLog.add("> no free ports -- trying use default " + Constants.CLIENT_PORT);
            seederPort = Constants.CLIENT_PORT;
            e.printStackTrace();
        }
        mainLog.add("> dump folder is " + loadFolderPath);
        leecher = new Leecher(loadFolderPath, broker, tasksPool, files, mainLog);
        seeder = new Seeder(seederPort, broker, tasksPool, files, mainLog);
        clientUpdater = new ClientUpdater(this.outputStream, seederPort, broker, leecher, files, mainLog, secondLog);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input folder for downloads:");
        String folder = scanner.nextLine();
        System.out.println("Input server ip (localhost / 4 bytes):");
        String serverIp = scanner.nextLine();
        new Client(System.in, System.out, folder, serverIp, Constants.SERVER_TORRENT_PORT).run();
    }

    @Override
    public void run() {
        deserialize();
        brokerPool.submit(broker);
        guysPool.submit(leecher);
        guysPool.submit(seeder);
        guysPool.submit(clientUpdater);
        if (!files.isEmpty()) {
            clientUpdater.update();
        }
        isRunning = true;
        processInput();
        processExit();
    }

    public void uploadFile(String filePath) throws IOException {
        processUpload(new String[]{"upload", filePath});
    }

    public void loadFile(Integer id) {
        processLoad(new String[]{"load", id.toString()});
    }

    public boolean isRunning() {
        return isRunning;
    }

    private void processInput() {
        Scanner scanner = new Scanner(this.inputStream);
        mainLog.add("> print `help` to help");
        try {
            while (alive && !broker.isInterrupted()) {
                String[] command = scanner.nextLine().split(" ");
                if (command.length == 0) {
                    continue;
                }
                switch (command[0]) {
                    case LIST:
                        processList();
                        break;
                    case UPLOAD:
                        processUpload(command);
                        break;
                    case LOAD:
                        processLoad(command);
                        break;
                    case SOURCES:
                        processSources(command);
                        break;
                    case HELP:
                        processHelp();
                        break;
                    case EXIT:
                        processExit();
                        break;
                    default:
                        badCommand();
                        break;
                }
            }
        } catch (IOException e) {
            mainLog.add("CLIENT EXCEPTION");
            e.printStackTrace();
        }
    }

    private void processList() {
        ServerRequest request = new ServerRequest(buildRequest(getListRequest()));
        broker.addRequest(request);
        Torrent.TrackerResponse trackerResponse = request.getResponse();
        Torrent.ListResponse response = trackerResponse.getList();
        StringBuilder logMessage = new StringBuilder();
        logMessage.append(String.format("Files count: %d", response.getCount()));
        for (Torrent.ListResponse.File file : response.getFilesList()) {
            logMessage.append(
                String.format("\n%d -- %s -- %d", file.getId(), file.getName(), file.getSize())
            );
        }
        mainLog.add(logMessage.toString());
    }

    protected void processUpload(String[] command) throws IOException {
        if (command.length != 2) {
            badCommand();
        } else {
            String fileName = FileInfo.createFileNameFromPath(command[1]);
            if (fileName == null || !new File(command[1]).exists()) {
                mainLog.add("> incorrect file path/name");
            } else if (uploadedFiles.contains(fileName)) {
                mainLog.add("> file already uploaded");
            } else {
                uploadedFiles.add(fileName);
                seeder.uploadRequest(fileName, command[1]);
                clientUpdater.update(); // update after upload
            }
        }
    }

    protected void processLoad(String[] command) {
        if (command.length != 2) {
            badCommand();
        } else if (command[1].equals(STATUS)) {
            leecher.loadStatus();
        } else {
            Integer id = TorrentUtils.getId(command[1]);
            if (id == null) {
                mainLog.add("> second argument isn't integer");
            } else if (files.containsKey(id)) {
                FileInfo file = files.get(id);
                if (file.isLoaded() || uploadedFiles.contains(file.getFilePath())) {
                    mainLog.add("> file already loaded");
                } else {
                    leecher.loadFile(id);
                }
            } else {
                leecher.loadFile(id);
            }
        }
    }

    void processSources(String[] command) {
        if (command.length != 2) {
            badCommand();
            return;
        }
        Integer id = TorrentUtils.getId(command[1]);
        if (id != null) {
            sourcesRequest(id);
        } else {
            mainLog.add("> second argument isn't integer");
        }
    }

    public void sourcesRequest(Integer id) {
        ServerRequest request = new ServerRequest(buildRequest(getSourcesRequest(id)));
        broker.addRequest(request); // add to broker
        Torrent.TrackerResponse trackerResponse = request.getResponse();
        Torrent.SourcesResponse response = trackerResponse.getSource();
        StringBuilder logMessage = new StringBuilder();
        logMessage.append(String.format("Total seeders: %d", response.getSize()));
        for (Torrent.SourcesResponse.Client client : response.getClientsList()) {
            String ip = null;
            try {
                ip = InetAddress.getByAddress(client.getIp().toByteArray()).toString();
            } catch (UnknownHostException e) {
                // ignore
            }
            logMessage.append(String.format("\n\t %s:%d", ip, client.getPort()));
        }
        mainLog.add(logMessage.toString());
    }

    public void processExit() {
        alive = false;
        serialize();
        clientUpdater.interrupt();
        leecher.interrupt();
        seeder.interrupt();
        broker.interrupt();
        guysPool.shutdown();
        brokerPool.shutdown();
        tasksPool.shutdown();
    }

    private void processHelp() {
        String logMessage = "Supported commands:\n" +
                "\tlist \t\t\t\t-- get all files from torrent tracker\n" +
                "\tupload file_path\t-- upload file to tracker\n" +
                "\tupload status \t\t-- show all current uploads\n" +
                "\tload id\t\t\t\t-- download file from tracker by id\n" +
                "\tload status \t\t-- show all current downloads\n" +
                "\texit\t\t\t\t-- close application\n";
        mainLog.add(logMessage);
    }

    private void badCommand() {
        mainLog.add("Bad command, use `help`");
    }

    public Queue<String> getSecondLog() {
        return secondLog;
    }

    public ConcurrentHashMap<Integer, FileInfo> getFiles() {
        return files;
    }

    public String getServerIp() {
        return broker.getServerIp();
    }

    public int getServerPort() {
        return broker.getServerPort();
    }

    @Override
    public void serialize() {
        if (files.isEmpty()) {
            return;
        }
        File file = new File(loadFolderPath + File.separator + DUMP_FILE);
        try {
            DataOutputStream outputStream = new DataOutputStream(new FileOutputStream(file));
            file.createNewFile();
            outputStream.writeUTF(getServerIp());
            outputStream.writeInt(getServerPort());
            outputStream.writeInt(files.size());
            for (FileInfo fileInfo : files.values()) {
                fileInfo.serialize(outputStream);
            }
            outputStream.close();
            mainLog.add("> info about " + files.size() + " files was dumped");
        } catch (IOException e) {
            mainLog.add("SERIALIZATION EXCEPTION");
            e.printStackTrace();
        }
    }

    @Override
    public void deserialize() {
        File file = new File(loadFolderPath + File.separator + DUMP_FILE);
        if (!file.exists() || file.length() == 0) {
            mainLog.add("> dump file doesn't exist or empty");
            return;
        }
        try {
            DataInputStream inputStream = new DataInputStream(new FileInputStream(file));
            String fileServerIp = inputStream.readUTF();
            int serverPort = inputStream.readInt();
            if (!checkIpPortFile(fileServerIp, serverPort)) {
                mainLog.add("> dump file is from other tracker");
                inputStream.close();
                file.delete();
                return;
            }
            int filesCount = inputStream.readInt();
            for (int i = 0; i < filesCount; ++i) {
                FileInfo fileInfo = FileInfo.deserialize(inputStream);
                if (new File(fileInfo.getFilePath()).exists()) {
                    files.put(fileInfo.getId(), fileInfo);
                }
            }
            inputStream.close();
            mainLog.add("> info about " + files.size() + " files was loaded");
        } catch (IOException e) {
            mainLog.add("DESERIALIZATION EXCEPTION");
            e.printStackTrace();
        }
    }

    public boolean checkIpPortFile(String ip, int port) {

        List<String> localhostIps = Arrays.asList("localhost", "127.0.0.1");
        if (port != getServerPort()) {
            return false;
        }
        if (localhostIps.contains(ip) && localhostIps.contains(getServerIp())) {
            return true;
        }
        return ip.equals(getServerIp());
    }
}
