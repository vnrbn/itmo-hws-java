package ru.ifmo.java.torrent.client;

import com.google.protobuf.ByteString;
import ru.ifmo.java.torrent.files.FileInfo;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.requests.ClientRequester;
import ru.ifmo.java.torrent.requests.ServerRequester;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class SeedWorker implements Runnable, ClientRequester, ServerRequester, ConsolePrinter {
    private final Socket socket;
    private final InputStream input;
    private final OutputStream output;
    private final Queue<String> log;
    private final ConcurrentHashMap<Integer, FileInfo> files;
    private final Set<Integer> currentIds = new HashSet<>();

    private boolean isInterrupted = false;
    private long lastUpdate = System.currentTimeMillis();

    public SeedWorker(
        Socket socket,
        ConcurrentHashMap<Integer, FileInfo> files,
        Queue<String> log
    ) throws IOException {
        this.socket = socket;
        this.input = socket.getInputStream();
        this.output = socket.getOutputStream();
        this.files = files;
        this.log = log;
    }

    @Override
    public void run() {
        try {
            while (!isInterrupted && !socket.isClosed()) {
                Torrent.ClientRequest request = receiveRequest();
                switch (request.getRequestCase()) {
                    case GET:
                        processGet(request.getGet());
                        break;
                    case STAT:
                        processStat(request.getStat());
                        break;
                    default:
                        break;
                }
            }
        } catch (SocketException e) {
            isInterrupted = true;
            log.add("> seed worker for " + socket.getRemoteSocketAddress().toString() + " closed");
        } catch (IOException e) {
            log.add("> SEED WORKER EXCEPTION FOR " + socket.getRemoteSocketAddress().toString());
            e.printStackTrace();
        } finally {
            currentIds.forEach(id -> {
                FileInfo file = files.get(id);
                if (file != null) {
                    file.unregisterSeedWorker();
                }
            });
        }
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void interrupt() {
        isInterrupted = true;
        try {
            if (!socket.isClosed()) {
                socket.close();
            }
        } catch (IOException e) {
            // ignore
        }
    }

    private void processStat(Torrent.StatRequest request) throws IOException {
        Integer id = request.getId();
        Torrent.StatResponse.Builder response = Torrent.StatResponse.newBuilder();
        if (files.containsKey(id)) {
            response.addAllParts(files.get(id).getLoadedBlocks());
            response.setCount(response.getPartsCount());
        } else {
            response.setCount(0);
        }
        getStatClientResponse(response.build()).writeDelimitedTo(output);
        lastUpdate = System.currentTimeMillis();
    }

    private void processGet(Torrent.GetRequest request) throws IOException {
        Integer fileId = request.getId();
        Integer blockId = request.getPart();
        Torrent.GetResponse.Builder response = Torrent.GetResponse.newBuilder();
        if (files.containsKey(fileId)) {
            FileInfo fileInfo = files.get(fileId);
            if (!currentIds.contains(fileId)) {
                currentIds.add(fileId);
                fileInfo.registerSeedWorker();
            }
            byte[] block = fileInfo.getBlock(blockId); // !!! blockId != fileId
            response.setContent(ByteString.copyFrom(block));
        } else {
            response.setContent(ByteString.copyFrom(new byte[0]));
        }
        getGetClientResponse(response.build()).writeDelimitedTo(output);
        lastUpdate = System.currentTimeMillis();
    }

    private Torrent.ClientRequest receiveRequest() throws IOException {
        return Torrent.ClientRequest.parseDelimitedFrom(input);
    }
}
