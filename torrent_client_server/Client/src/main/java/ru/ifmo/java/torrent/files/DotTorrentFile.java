package ru.ifmo.java.torrent.files;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class DotTorrentFile {
    private byte[] ip = new byte[4];
    private int port;
    private int id;
    private String name;
    private long size;

    public DotTorrentFile() {}

    public static DotTorrentFile parse(File file) {
        DotTorrentFile dotTorrentFile = new DotTorrentFile();
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(file));
            dis.read(dotTorrentFile.getIp());
            dotTorrentFile.setPort(dis.readInt());
            dotTorrentFile.setId(dis.readInt());
            dotTorrentFile.setName(dis.readUTF());
            dotTorrentFile.setSize(dis.readLong());
        } catch (IOException e) {
            e.printStackTrace();
            dotTorrentFile = null;
        }
        return dotTorrentFile;
    }

    public byte[] getIp() {
        return ip;
    }

    public String getStringIp() throws UnknownHostException {
        return InetAddress.getByAddress(ip).getHostAddress();
    }

    public void setIp(byte[] ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
