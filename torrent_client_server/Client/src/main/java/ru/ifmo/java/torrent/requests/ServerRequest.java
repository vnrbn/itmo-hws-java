package ru.ifmo.java.torrent.requests;

import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ServerRequest implements Request, ServerRequester, ConsolePrinter {
    private final Lock requestLock = new ReentrantLock();
    private final Condition ready = requestLock.newCondition();

    private final Torrent.TrackerRequest request;
    private Torrent.TrackerResponse response = null;

    private volatile boolean isReady = false;

    public ServerRequest(Torrent.TrackerRequest request) {
        this.request = request;
    }

    @Override
    public void process(InputStream input, OutputStream output) throws IOException {
        if (!isReady) {
            try {
                requestLock.lockInterruptibly();
                response = requestResponse(input, output, request);
            } catch (InterruptedException e) {
                // ignore
            } finally {
                isReady = true;
                ready.signalAll();
                requestLock.unlock();
            }
        }
    }

    public Torrent.TrackerResponse getResponse() {
        if (!isReady) {
            requestLock.lock();
            try {
                while (!isReady) {
                    ready.await();
                }
            } catch (InterruptedException e) {
                // ignore
            } finally {
                isReady = true;
                requestLock.unlock();
            }
        }
        return response;
    }

    public boolean isReady() {
        return isReady;
    }
}
