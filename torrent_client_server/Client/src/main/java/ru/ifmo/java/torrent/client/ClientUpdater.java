package ru.ifmo.java.torrent.client;

import ru.ifmo.java.torrent.files.FileInfo;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.requests.ServerBroker;
import ru.ifmo.java.torrent.requests.ServerRequest;
import ru.ifmo.java.torrent.requests.ServerRequester;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Queue;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import static ru.ifmo.java.torrent.Constants.UPDATE_REQUEST_RANGE;

public class ClientUpdater implements Runnable, ServerRequester, ConsolePrinter {
    private final long MISSED_BLOCKS_CHECK_TIME = 60 * 1000;

    private final int port;
    private final Leecher leecher;
    private final ServerBroker broker;
    private final ConcurrentHashMap<Integer, FileInfo> files;

    private boolean needUpdate = false;
    private boolean isInterrupted = false;

    private int lastSizeUpdated = 0;
    private long lastTimeUpdateRequest = 0;
    private long lastTimeMissedBlocksCheck = 0;

    private final OutputStream outputStream;
    private final OutputStreamWriter writer;
    private final Queue<String> consoleLog;
    private final Queue<String> guiLog;

    public ClientUpdater(
        OutputStream outputStream,
        int seederPort,
        ServerBroker broker,
        Leecher leecher,
        ConcurrentHashMap<Integer, FileInfo> files,
        Queue<String> log,
        Queue<String> guiLog
    ) {
        this.outputStream = outputStream;
        this.writer = new OutputStreamWriter(this.outputStream);
        this.port = seederPort;
        this.broker = broker;
        this.leecher = leecher;
        this.files = files;
        this.consoleLog = log;
        this.guiLog = guiLog;
    }

    @Override
    public void run() {
        consoleLog.add("> updater is ready");
        while (!isInterrupted) {
            updateLog();
            updateRequest();
            updateMissedBlocks();
        }
    }

    public void interrupt() {
        isInterrupted = true;
    }

    public void updateRequest() {
        if (!needUpdate && System.currentTimeMillis() - lastTimeUpdateRequest < UPDATE_REQUEST_RANGE && lastSizeUpdated == files.size()) {
            return;
        }
        ServerRequest request = new ServerRequest(buildRequest(getUpdateRequest(port, new HashSet<>(files.keySet()))));
        broker.addRequest(request);
        Torrent.TrackerResponse trackerResponse = request.getResponse();
        if (trackerResponse.getUpdate().getStatus()) {
            consoleLog.add("> update request for " + files.size() + " files submitted, status: OK");
            lastTimeUpdateRequest = System.currentTimeMillis();
            lastSizeUpdated = files.size();
        } else {
            consoleLog.add("> update request submitted, status: NOT OK");
        }
        needUpdate = false;
    }

    private void updateLog() {
        try {
            while (!consoleLog.isEmpty()) {
                String message = consoleLog.poll();
                guiLog.add(message);
                writer.write(message + "\n");
                writer.flush();
            }
        } catch (IOException e) {
            // ignore
        }
    }

    private void updateMissedBlocks() {
        if (System.currentTimeMillis() - lastTimeMissedBlocksCheck <= MISSED_BLOCKS_CHECK_TIME) {
            return;
        }
        for (FileInfo file : files.values()) {
            if (!file.isLoaded() && file.getLeechWorkersCount() == 0) {
                leecher.loadFile(file.getId());
            }
        }
        lastTimeMissedBlocksCheck = System.currentTimeMillis();
    }


    public void update() {
        needUpdate = true;
    }
}
