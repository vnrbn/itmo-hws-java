package ru.ifmo.java.torrent.client;

import ru.ifmo.java.torrent.files.FileInfo;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.requests.ServerBroker;
import ru.ifmo.java.torrent.requests.ServerRequest;
import ru.ifmo.java.torrent.requests.ServerRequester;
import ru.ifmo.java.torrent.utils.ConsolePrinter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;

public class Seeder implements Runnable, ServerRequester, ConsolePrinter {
    private final int port;
    private final ServerBroker broker;
    private final ExecutorService pool;
    private final ConcurrentHashMap<Integer, FileInfo> files;
    private final List<SeedWorker> workers = new LinkedList<>();

    private final Queue<String> log;

    private ServerSocket socket = null;
    private boolean isInterrupted = false;

    public Seeder(
            int port,
            ServerBroker broker,
            ExecutorService pool,
            ConcurrentHashMap<Integer, FileInfo> files,
            Queue<String> log
    ) {
        this.port = port;
        this.broker = broker;
        this.pool = pool;
        this.files = files;
        this.log = log;
    }

    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            log.add("> seeder is ready by address: " +
                    serverSocket.getInetAddress().getHostAddress() + ":" + port);
            this.socket = serverSocket;
            while (!isInterrupted && !serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                log.add("> new peer connected " + socket.getRemoteSocketAddress().toString());
                SeedWorker worker = new SeedWorker(socket, files, log);
                workers.add(worker);
                pool.submit(worker);
            }
            workers.forEach(SeedWorker::interrupt);
        } catch (SocketException e) {
            log.add("SEEDER SOCKET CLOSED");
        } catch (IOException e) {
            e.printStackTrace();
            log.add("> seeder is dead");
        } finally {
            isInterrupted = true;
        }
    }

    public void interrupt() {
        isInterrupted = true;
        try {
            if (socket != null && !socket.isClosed()) {
                socket.close();
            }
        } catch (IOException e) {
            // ignore
        }
    }

    public void uploadRequest(String fileName, String filePath) throws IOException {
        ServerRequest serverRequest = new ServerRequest(buildRequest(getUploadRequest(fileName, filePath)));
        broker.addRequest(serverRequest);
        Torrent.TrackerResponse trackerResponse = serverRequest.getResponse();
        Torrent.UploadResponse response = trackerResponse.getUpload();
        FileInfo fileInfo = new FileInfo(response.getId(), filePath, fileName, null, false);
        files.put(response.getId(), fileInfo);
        log.add("> file was added to tracker [id: " + response.getId() + ", name: " + fileName + "]");
    }
}
