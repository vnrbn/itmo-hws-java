package ru.ifmo.java.torrent.client;

import ru.ifmo.java.torrent.files.FileInfo;
import ru.ifmo.java.torrent.protocol.Torrent;
import ru.ifmo.java.torrent.requests.ClientRequest;
import ru.ifmo.java.torrent.requests.ClientRequester;
import ru.ifmo.java.torrent.utils.ConsolePrinter;
import ru.ifmo.java.torrent.utils.Source;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

public class LeechWorker implements Runnable, ClientRequester, ConsolePrinter {
    private final Source source;
    private final FileInfo fileInfo;
    private final Queue<String> log;

    private final List<Integer> blocks = new LinkedList<>();
    private final List<Integer> badBlocks = new LinkedList<>();
    private final List<Integer> goodBlocks = new LinkedList<>();

    private Socket socket;

    public LeechWorker(
            Source source,
            Collection<Integer> blocks,
            FileInfo fileInfo,
            Queue<String> log
    ) {
        this.source = source;
        this.blocks.addAll(blocks);
        this.fileInfo = fileInfo;
        this.log = log;
    }

    @Override
    public void run() {
        try (Socket socket = new Socket(InetAddress.getByAddress(source.getIp()), source.getPort())) {
            this.socket = socket;
            for (Integer blockId : blocks) {
                ClientRequest request = createGetRequest(blockId);
                request.process(socket.getInputStream(), socket.getOutputStream());
                processResponse(blockId, request.getResponse());
            }
            log.add("> loaded blocks [good: " + goodBlocks.size() + ", bad: " + badBlocks.size() + "] from "
                    + socket.getInetAddress().toString() + ":" + socket.getPort() + " for " + fileInfo.toString());
        } catch (SocketException e) {
            log.add("LEECH WORKER SOCKET WAS CLOSED FOR " + socket.getRemoteSocketAddress().toString());
        } catch (IOException e) {
            log.add("LEECH WORKER EXCEPTION FOR" + fileInfo.toString());
            e.printStackTrace();
        } finally {
            this.fileInfo.unregisterLeechWorker();
        }
    }

    public void interrupt() {
        try {
            socket.close();
        } catch (IOException e) {
            // ignore
        }
    }

    private void processResponse(Integer blockId, Torrent.ClientResponse clientResponse) throws IOException {
        if (clientResponse == null) {
            badBlocks.add(blockId);
            return;
        }
        Torrent.GetResponse getResponse = clientResponse.getGet();
        if (getResponse.getContent().size() == 0) {
            badBlocks.add(blockId);
        } else if (fileInfo.writeBlock(blockId, getResponse.getContent().toByteArray())) {
            goodBlocks.add(blockId);
        }
    }

    private ClientRequest createGetRequest(Integer blockId) {
        return new ClientRequest(getGetClientRequest(getGetRequest(fileInfo.getId(), blockId)));
    }
}
