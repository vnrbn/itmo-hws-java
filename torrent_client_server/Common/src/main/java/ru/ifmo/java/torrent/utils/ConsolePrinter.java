package ru.ifmo.java.torrent.utils;

public interface ConsolePrinter {
    default void cout(String message) {
        cout(message, true);
    }

    default void cout(String message, boolean newLine) {
        if (newLine) {
            System.out.println(message);
        } else {
            System.out.print(message);
        }
    }
}
