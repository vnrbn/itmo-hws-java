package ru.ifmo.java.torrent.client;

import com.google.protobuf.ByteString;
import ru.ifmo.java.torrent.protocol.Torrent;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class ClientInfo {
    private final byte[] ip;
    private final int port;

    public ClientInfo(byte[] ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public byte[] getIp() {
        return ip;
    }

    public Torrent.SourcesResponse.Client getSourceResponseFormat() {
        return Torrent.SourcesResponse.Client.newBuilder()
                .setIp(ByteString.copyFrom(ip))
                .setPort(port)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ClientInfo) {
            return Arrays.equals(((ClientInfo) o).ip, this.ip) && ((ClientInfo) o).port == this.port;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(ip) + (port * 17) % 67733;
    }

    @Override
    public String toString() {
        String clientString = null;
        try {
            clientString = InetAddress.getByAddress(ip).getHostAddress();
        } catch (UnknownHostException e) {
            clientString = "bad_ip";
        }
        return clientString + ":" + port;
    }
}
