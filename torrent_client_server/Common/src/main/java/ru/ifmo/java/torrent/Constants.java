package ru.ifmo.java.torrent;

public class Constants {
    // server
    public static final int SERVER_WEB_PORT = 8080;
    public static final int SERVER_TORRENT_PORT = 8081;
    public static final long CLIENT_UPDATE_RANGE = 6 * 60 * 1000;

    // client
    public static final int CLIENT_PORT = 45678;
    public static final int CLIENT_THREADS_CNT = 2;
    public static final int LEECHERS_PER_FILE = 4;
    public static final long BLOCK_SIZE = 5 * 1024 * 1024; // MB
    public static final long UPDATE_REQUEST_RANGE = 1 * 60 * 1000; // minutes

    // requests
    public static final int LIST_BYTES = 1;
    public static final int UPLOAD_BYTES = 2;
    public static final int SOURCES_BYTES = 3;
    public static final int UPDATE_BYTES = 4;
    public static final int STAT_BYTES = 1;
    public static final int GET_BYTES = 2;
}
