package ru.ifmo.java.torrent.utils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class Source {
    private final byte[] ip;
    private final int port;

    public Source(byte[] ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public byte[] getIp() {
        return ip;
    }

    public byte[] serialize() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(new ByteArrayOutputStream());
        try {
            dos.writeInt(port);
            dos.write(ip);
            dos.flush();
        } catch (IOException e) {
            return new byte[0];
        }
        return bos.toByteArray();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Source) {
            return ((Source) o).ip == this.ip && ((Source) o).port == this.port;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(ip) + (port * 17) % 67733;
    }
}
