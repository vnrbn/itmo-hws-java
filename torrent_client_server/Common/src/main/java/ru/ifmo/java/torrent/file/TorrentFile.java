package ru.ifmo.java.torrent.file;

import ru.ifmo.java.torrent.client.ClientInfo;
import ru.ifmo.java.torrent.protocol.Torrent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public class TorrentFile {
    private final Integer id;
    private final String name;
    private final Long size;
    private final ConcurrentHashMap<ClientInfo, Long> owners = new ConcurrentHashMap<>();

    public TorrentFile(Integer id, String name, Long size) {
        this.id = id;
        this.name = name;
        this.size = size;
    }

    public Torrent.ListResponse.File getListResponseFormat() {
        return Torrent.ListResponse.File.newBuilder()
                .setId(id)
                .setName(name)
                .setSize(size)
                .build();
    }

    public Torrent.UploadResponse getUploadResponseFormat() {
        return Torrent.UploadResponse.newBuilder()
                .setId(id)
                .build();
    }

    public Long getSize() {
        return size;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public ConcurrentHashMap<ClientInfo, Long> getOwners() {
        return owners;
    }

    public byte[] serialize() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(new ByteArrayOutputStream());
        try {
            dos.writeInt(id);
            dos.writeUTF(name);
            dos.writeLong(size);
            dos.flush();
        } catch (IOException e) {
            return new byte[0];
        }
        return bos.toByteArray();
    }

    @Override
    public String toString() {
        return "[id: " + id + ", name: " + name + ", size: " + size + "]";
    }
}
