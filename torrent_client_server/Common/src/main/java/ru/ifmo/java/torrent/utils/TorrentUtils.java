package ru.ifmo.java.torrent.utils;

import java.io.IOException;
import java.net.ServerSocket;

public abstract class TorrentUtils {

    public static int findFreePort() throws IOException {
        ServerSocket s = new ServerSocket(0);
        int port = s.getLocalPort();
        s.close();
        return port;
    }

    public static Integer getId(String idString) {
        Integer id = null;
        try {
            id = Integer.parseInt(idString);
        } catch (NumberFormatException e) {
            id = null;
        }
        return id;
    }
}
